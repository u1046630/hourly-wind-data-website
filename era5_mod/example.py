import era5_mod
from datetime import datetime

# config maps a directory path (from data_dir root) to a tuple:
# (date_start, date_end, level, parameter_list, num_npz_files)

# the directory should contain .npy files, each with an equal number of
# arrays inside. each .npy array should contain the data for one N320
# grid point and parameter ('u' is eastward-wind, 'v' is northward-wind, 
# 't' is temperature), for each hour between date_start and date_end,
# at the ERA5 model level specified.

data_dir = '/media/liam/drive1/era5/'
year = lambda x: datetime(x, 1, 1)
config = {
    'level131-2010-2019/data/': (year(2010), year(2020), 131, ('u','v'), 484),
    'level133-2010-2019/data/': (year(2010), year(2020), 133, ('u','v','t'), 484),
}

era5_reader = era5_mod.Reader(data_dir, config)
data = era5_reader.wind_speed(datetime(2010, 1, 1), datetime(2011, 1, 1), 133, 10.0, 20.0, format='dataframe')

print(data)