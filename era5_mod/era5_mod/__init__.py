__version__ = '0.1.0'

from .reader import Reader
from .s3_reader import Reader as S3Reader
from .new_reader import Reader as NewReader