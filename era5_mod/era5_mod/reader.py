
import numpy as np, pandas as pd, os
from datetime import datetime, timedelta

from .n320 import N320

NUM_N320_PTS = 542080

class Reader(object):
    '''class for reading downloaded ERA5 data that has been processed into
    .npz files'''

    def __init__(self, data_dir, config):
        self.data_dir = data_dir
        self.config = config

        n320_dir = os.path.join(os.path.dirname(__file__), 'data/n320/')
        self.n320 = N320(n320_dir)

    def __get_fname(self, path, i_n320):
        '''path to npz file that stores the particular n320 grid point data'''
        num_files = self.config[path][4]
        num_n320_pts_per_file = NUM_N320_PTS // num_files
        i_file = i_n320 // num_n320_pts_per_file
        i_n320_start = i_file * num_n320_pts_per_file
        i_n330_end = i_n320_start + num_n320_pts_per_file - 1
        return path + f'{i_n320_start}_{i_n330_end}.npz'

    def __df_format(date_from, date_to, **kwargs):
        time = np.arange(date_from, date_to, timedelta(hours=1))
        df = pd.DataFrame(kwargs, index=time)
        df.index.name = 'datetime'
        return df

    def wind_speed_at_height(self, date_from, date_to, height, lat, lon, interpolated=True, format=None):
        wind131 = self.wind_speed_at_level(date_from, date_to, 131, lat, lon, interpolated=interpolated)
        wind133 = self.wind_speed_at_level(date_from, date_to, 133, lat, lon, interpolated=interpolated)
        wind135 = self.wind_speed_at_level(date_from, date_to, 135, lat, lon, interpolated=interpolated)

        x = np.array([169.5, 106.54, 53.92])
        y = np.array([wind131, wind133, wind135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        data = a + b * np.log(height)
        if format == 'dataframe':
            data = Reader.__df_format(date_from, date_to, wind_speed=data)
        return data
        

    def wind_speed_at_level(self, date_from, date_to, level, lat, lon, interpolated=True, format=None):
        u = self.read_data(date_from, date_to, level, 'u', lat, lon, interpolated)
        v = self.read_data(date_from, date_to, level, 'v', lat, lon, interpolated)
        data = np.sqrt(u**2 + v**2)
        if format == 'dataframe':
            data = Reader.__df_format(date_from, date_to, wind_speed=data)
        return data

    def read_data(self, date_from, date_to, level, param, lat=None, lon=None, interpolated=False, n320_index=None):
        '''
        from_date, to_date: datetime objects, inclusive and exclusive
        level: int, ERA5 model level
        param: str, 'u' for eastward wind, 'v' for northward wind, 't' for temperature
        lat, lon: floats, WGS84
        interpolated: bool, False means use the closest n320 grid point
        '''
        # date_from and to_date represent the requested period
        # date1 and date2 represent the period STORED in a particular download directory
        # dateA and dateB represent the period NEEDED from a particular download directory
        # all of them are inclusive, exclusive respectively
            
        periods = {} # similar format as config (without level and param)
        temp_date = date_from
        while temp_date < date_to:
            for path,(date1,date2,lvl,params,num_files) in self.config.items():
                if level == lvl and param in params:
                    if temp_date >= date1 and temp_date < date2:
                        periods[path] = (temp_date, min(date2, date_to))
                        temp_date = date2
                        break
            else:
                raise Exception('[!] no data avaliable (date={}, param={})'.format(
                    temp_date, param
                ))
            
        hour = timedelta(hours=1)
        data = np.zeros((date_to - date_from) // hour) ### dtype='float16' to keep small
        
        for path,(dateA,dateB) in periods.items():
            date1 = self.config[path][0] # start of STORED downloaded
            
            nhrs = (dateB - dateA) // hour
            i_src = (dateA - date1) // hour
            i_dst = (dateA - date_from) // hour
            
            if n320_index:
                weights = {n320_index: 1.0}
            elif interpolated:
                weights = self.n320.latlon_to_n320_interpolated(lat, lon)
            else:
                weights = {self.n320.latlon_to_n320(lat, lon): 1.0}
                
                
            for i_n320, weight in weights.items():
                fname = self.data_dir + self.__get_fname(path, i_n320)
                array_name = '{}_{}'.format(i_n320, param.lower())
                array = np.load(fname)[array_name] * weight
                data[i_dst:(i_dst+nhrs)] += array[i_src:(i_src+nhrs)] 
            
        return data
