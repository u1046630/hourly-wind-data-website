
import numpy as np, pandas as pd
import boto3, os, io
from datetime import datetime, timedelta

from .n320 import N320

class Reader(object):
    '''class for reading downloaded ERA5 data that has been processed into
    .npz files'''

    def __init__(self, data_dir, config, dir_type='s3'):
        assert(dir_type == 's3') # local storage not implemented
        self.data_dir = data_dir
        self.dir_type = dir_type
        self.config = config
        n320_dir = os.path.join(os.path.dirname(__file__), 'data/n320/')
        self.n320 = N320(n320_dir)

    def __load_array(self, path, i_n320):
        '''path to npz file that stores the particular n320 grid point data'''
        key = f'{path}{i_n320}.npy'
        npy = io.BytesIO(self.data_dir.Object(key).get()['Body'].read())
        return np.load(npy)

    def __df_format(date_from, date_to, **kwargs):
        time = np.arange(date_from, date_to, timedelta(hours=1))
        df = pd.DataFrame(kwargs, index=time)
        df.index.name = 'datetime'
        return df

    def wind_speed(self, date_from, date_to, level, lat, lon, interpolated=False, format=None):
        u = self.read_data(date_from, date_to, level, 'u', lat, lon, interpolated)
        v = self.read_data(date_from, date_to, level, 'v', lat, lon, interpolated)
        data = (u**2 + v**2)**0.5
        if format == 'dataframe':
            data = Reader.__df_format(date_from, date_to, wind_speed=data)
        return data

    def read_data(self, date_from, date_to, level, param, lat=None, lon=None, interpolated=False, n320_index=None):
        '''
        from_date, to_date: datetime objects, inclusive and exclusive
        level: int, ERA5 model level
        param: str, 'u' for eastward wind, 'v' for northward wind, 't' for temperature
        lat, lon: floats, WGS84
        interpolated: bool, False means use the closest n320 grid point
        '''
        # date_from and date_to represent the requested period
        # date1 and date2 represent the period STORED in a particular download directory
        # dateA and dateB represent the period NEEDED from a particular download directory
        # all of them are inclusive, exclusive respectively
            
        periods = {} # similar format as config (without level and param)
        temp_date = date_from
        while temp_date < date_to:
            for path,(date1,date2,lvl,params) in self.config.items():
                if level == lvl and param in params:
                    if temp_date >= date1 and temp_date < date2:
                        periods[path] = (temp_date, min(date2, date_to))
                        temp_date = date2
                        break
            else:
                raise Exception('[!] no data avaliable (date={}, param={})'.format(
                    temp_date, param
                ))
            
        hour = timedelta(hours=1)
        data = np.zeros((date_to - date_from) // hour, dtype='float16')
        
        for path,(dateA,dateB) in periods.items():
            date1 = self.config[path][0] # start of STORED downloaded
            
            nhrs = (dateB - dateA) // hour
            i_src = (dateA - date1) // hour
            i_dst = (dateA - date_from) // hour
            
            if interpolated:
                weights = self.n320.latlon_to_n320_interpolated(lat, lon)
            elif lat is not None and lon is not None:
                weights = {self.n320.latlon_to_n320(lat, lon): 1.0}
            else:
                weights = {n320_index: 1.0}
                
            for i_n320, weight in weights.items():
                array = self.__load_array(path, i_n320)
                data[i_dst:(i_dst+nhrs)] += array[i_src:(i_src+nhrs)]*weight
                
        return data