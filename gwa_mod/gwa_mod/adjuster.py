from zipfile import ZipFile
import pandas as pd, numpy as np
import scipy.stats as stats

class Gwc_File(object):
    '''class for parsing a Generalised Wind Climate (GWC) file from the Global Wind Atlas'''

    def __line_to_nums(line, dtype='float'):
        return np.array([x for x in line.split(' ') if x != ''], dtype=dtype)

    def __init__(self, fname):
        with open(fname, 'r') as f:
            lines = f.readlines()
        
        line = lines.pop(0)
        assert(line.startswith('Global Wind Atlas 3.0 (WRF 3-km)'))
        lat,lon,_ = line.split('<coordinates>')[-1].split('</coordinates>')[0].split(',')
        lat, lon = np.float(lat), np.float(lon)
        
        num_roughnesses, num_heights, num_sectors = Gwc_File.__line_to_nums(lines.pop(0), dtype='int')
        
        self.roughnesses = Gwc_File.__line_to_nums(lines.pop(0))
        self.heights = Gwc_File.__line_to_nums(lines.pop(0))
        
        self.f = np.full((num_roughnesses, num_sectors), np.nan)
        self.a = np.full((num_roughnesses, num_heights, num_sectors), np.nan)
        self.k = np.full((num_roughnesses, num_heights, num_sectors), np.nan)
        
        
        for i in range(num_roughnesses):
            self.f[i:] = Gwc_File.__line_to_nums(lines.pop(0))
            for j in range(num_heights):
                self.a[i,j,:] = Gwc_File.__line_to_nums(lines.pop(0))
                self.k[i,j,:] = Gwc_File.__line_to_nums(lines.pop(0))
                
        assert(len(lines) == 0)


class Gwa_Stats(object):
    '''class for storing Global Wind Atlas statistics (from plot data on right hand panel and GWC files)'''

    def __init__(self, gwc_fname, plots_fname, roughness, height):
        self.gwc_fname = gwc_fname
        self.plots_fname = plots_fname
        self.roughness = roughness
        self.height = height

        self.load_weibulls()
        with ZipFile(self.plots_fname) as plots_zip: 
            self.load_temporals(plots_zip)
            self.load_crosstable(plots_zip)
            self.load_mean(plots_zip)

    def load_mean(self, plots_zip):
        with plots_zip.open('windSpeed.csv') as f:
            df = pd.read_csv(f)
        self.mean = df.val.mean()

    def load_crosstable(self, plots_zip):
        hour_vect = np.arange(0, 24)
        month_vect = np.arange(1, 12+1)
        self.crosstable = np.zeros((len(month_vect), len(hour_vect)))
        
        with plots_zip.open('HourlyMonthlyData.csv') as f:
            df = pd.read_csv(f)
        for i in range(len(month_vect)):
            for j in range(len(hour_vect)):
                value = df[(df.month==month_vect[i]) & (df.hour==hour_vect[j])].iloc[0].value
                self.crosstable[i,j] = value

    def load_weibulls(self):
        gwc_file = Gwc_File(self.gwc_fname)

        #i = np.where(gwc_file.roughnesses == self.roughness)[0][0]
        #j = np.where(gwc_file.heights == self.height)[0][0]
        i = np.argmin(np.abs(gwc_file.roughnesses - self.roughness)) # pick the closest roughness (maybe make this log or something nicer)
        j = np.argmin(np.abs(gwc_file.heights - self.height))
        # print('roughness: ', gwc_file.roughnesses, self.roughness, i)
        # print('heights: ', gwc_file.heights, self.height, j)
        a = gwc_file.a[i,j,:] # weibull a parameter by sector
        k = gwc_file.k[i,j,:] # weibull k parameter by sector
        f = gwc_file.f[i,:] # frequency by sector
        
        # have to convert their sector (clockwise from north) 
        # to my sectors (anitclockwise from east)
        mapping = np.arange(3,3-12,-1)
        self.a = a.take(mapping)
        self.k = k.take(mapping)
        self.f = f.take(mapping)

        #print('GWC weibulls')
        #for i in range(12):
        #    print(f'{self.a[i]:5.2f} {self.k[i]:5.2f} {self.f[i]:5.2f}')

    def load_temporals(self, plots_zip):

        with plots_zip.open('HourlyData.csv') as f:
            df = pd.read_csv(f)
        self.hour_values = np.array(df['value'])
        self.hour_vector = np.array(df['time'])
        
        with plots_zip.open('MonthlyData.csv') as f:
            df = pd.read_csv(f)
        self.month_values = np.array(df['value'])
        self.month_vector = np.array(df['time'])
        
        with plots_zip.open('AnnualData.csv') as f:
            df = pd.read_csv(f)
        self.year_values = np.array(df['value'])
        self.year_vector = np.array(df['time'])


class Adjuster(object):
    '''class for adjusting hourly wind speeds to match Global Wind Atlas statistics'''

    def __init__(self, mode, wind, datetime, gwc_fname, plots_fname, roughness, height):
        '''
        wind is 1D array of wind speeds, or tuple of two wind speed arrays (u_wind, v_wind)
        datetime: 1D numpy arrays of the same length
        gwc_fname, plots_fname: file paths to the GlobalWindAtlas statistics
        '''
        self.mode = mode

        if mode == 'scalar':
            # only given wind speed, no direction, no direction-based adjustments can be made (wind rose graphs)
            self.wind = wind
            self.angle = None

        elif mode == 'vector':
            # wind speed and direction are given, direction-based adjustments can be made
            # wind is tuple (u_wind, v_wind)
            u_wind, v_wind = wind
            self.wind = (u_wind**2 + v_wind**2)**0.5
            self.angle = (np.degrees(np.arctan(v_wind/u_wind)) + 180.0 * (u_wind > 0.0)) % 360.0
            self.boundaries = np.linspace(0, 360, 12, endpoint=False) + 360/24

        else:
            raise Exception('[!] mode must be "scalar" or "vector"')

        self.time = pd.DatetimeIndex(datetime)
        self.wind_orig = self.wind.copy()

        self.gwa_stats = Gwa_Stats(gwc_fname, plots_fname, roughness, height)

    def auto_adjust(self):
        self.adjust_to_crosstable()
        self.adjust_to_mean()

        # if self.mode == 'vector':
        #   self.adjust_boundaries()
        #   self.adjust_to_weibulls()

    def quanify_adjustment(self):
        bias = np.mean(self.wind - self.wind_orig)
        error = np.std(self.wind - self.wind_orig - bias)
        print('[*] total adjustment: {:.2f} +/- {:.2f} m/s'.format(bias, error))
        return bias, error

    def adjust_to_weibulls(self):
        self.calc_sectors()
        self.calc_weibulls()

        new_wind = np.zeros(self.wind.shape)
        k = self.k / self.gwa_stats.k
        for i in range(12):
            mask = (self.sectors == i)
            new_wind += mask * (self.gwa_stats.a[i] * ((self.wind / self.a[i])**k[i]))
        
        self.wind = new_wind

    def adjust_boundaries(self):
        '''adjust the angle of the boundaries between sectors
        so that sector frequencies match self.gwa_stats.f'''
        self.calc_sectors()
        self.calc_weibulls()

        assert(self.angle.min() >= 0 and self.angle.max() < 360)

        # curve_x and curve_y form a sort of duration curve
        # point (x,y) on curve means for x% of the time angle < y
        curve_y = self.angle.copy()
        curve_y.sort()
        curve_x = np.linspace(0, 100, len(curve_y), endpoint=False)

        # boundaries are the angles between the 12 sectors on the wind rose (in degrees)
        # deltas are the amount these boundaries have to shift
        # by % of wind frequency (not angle), so use to above curve to convert back and forth
        show = lambda l: ', '.join([f'{x:5.1f}' for x in l])
        deltas = np.cumsum(self.f - self.gwa_stats.f)
        deltas -= np.mean(deltas)
        x = np.interp(self.boundaries, curve_y, curve_x)
        x = (x - deltas) % 100
        new_boundaries = np.interp(x, curve_x, curve_y)
        self.boundaries = new_boundaries


    def adjust_to_crosstable(self):
        self.calc_crosstable()

        new_wind = np.zeros(self.wind.shape)
        hour_vect = np.arange(0, 24)
        month_vect = np.arange(1, 12+1)
        
        for i in range(len(month_vect)):
            for j in range(len(hour_vect)):
                mask = (self.time.month == month_vect[i]) * (self.time.hour == hour_vect[j])
                new_wind += mask * self.wind * (self.gwa_stats.crosstable[i,j] / self.crosstable[i,j])
            
        self.wind = new_wind

    def adjust_to_mean(self):
        self.wind *= self.gwa_stats.mean / self.wind.mean()

    def calc_sectors(self):
        # sector1 lies between boundaries 0 and 1
        self.sectors = np.zeros(self.angle.shape, dtype='int')
        for i0 in range(12):
            i1 = (i0 + 1)%12
            b0, b1 = self.boundaries[i0],  self.boundaries[i1]
            if b0 < b1:
                self.sectors = self.sectors + ((self.angle >= b0) * (self.angle < b1)) * i1
            else:
                self.sectors = self.sectors + ((self.angle >= b0) + (self.angle < b1)) * i1
    
    def calc_crosstable(self):
        hour_vect = np.arange(0, 24)
        month_vect = np.arange(1, 12+1)
        
        self.crosstable = np.zeros((len(month_vect), len(hour_vect)))
        for i in range(len(month_vect)):
            for j in range(len(hour_vect)):
                weights = np.bitwise_and(self.time.month==month_vect[i], self.time.hour==hour_vect[j])
                if not np.any(weights):
                    raise Exception('not enough data to adjust to GWA statistic')
                self.crosstable[i,j] = np.average(self.wind, weights=weights)
        self.crosstable = self.crosstable / np.mean(self.crosstable)

    def calc_weibulls(self):
        self.a = np.full(12, np.nan)
        self.k = np.full(12, np.nan)
        self.f = np.full(12, np.nan)
        for i in range(12):
            indices = np.where(self.sectors == i)[0]
            self.f[i] = len(indices)
            temp = np.take(self.wind, indices)
            if len(temp) == 0:
                raise Exception('not enough data to adjust to GWA statistic')
            k,_,a = stats.weibull_min.fit(temp, floc=0)
            self.a[i] = a # scale parameter (roughly average wind speed in m/s)
            self.k[i] = k # shape parameter (high k means low variance of wind speeds)
        self.f = self.f / np.sum(self.f) * 100.0

        #print('ERA5 weibulls')
        #for i in range(12):
        #    print(f'{self.a[i]:5.2f} {self.k[i]:5.2f} {self.f[i]:5.2f}')

    def calc_temporals(self):
    
        hours = self.time.hour
        months = self.time.month
        years = self.time.year

        self.hour_vect = np.arange(hours.min(), hours.max() + 1)
        self.month_vect = np.arange(months.min(), months.max() + 1)
        self.year_vect = np.arange(years.min(), years.max() + 1)
        
        self.hour_values = np.zeros(hour_vect.shape)
        for i in range(len(self.hour_vect)):
            self.hour_values[i] = np.average(wind, weights=hours==hour_vect[i])
        self.hour_values /= np.mean(self.hour_values)
        
        self.month_values = np.zeros(month_vect.shape)
        for i in range(len(self.month_vect)):
            self.month_values[i] = np.average(wind, weights=months==month_vect[i])
        self.month_values /= np.mean(self.month_values)
        
        self.year_values = np.zeros(year_vect.shape)
        for i in range(len(self.year_vect)):
            self.year_values[i] = np.average(wind, weights=years==year_vect[i])
        self.year_values /= np.mean(self.year_values)

