

from datetime import datetime, timedelta
import glob
import pandas as pd, numpy as np

import era5_mod, gwa_mod

###############
### globals ###
###############

name, country = '-17.79,-62.654', 'coords'
roughness, height = 0.1, 100.0
date_start = datetime(2008, 1, 1)
date_end = datetime(2018, 1, 1)

globalwindatlas_dir = '/media/liam/drive1/globalwindatlas/'
plots_fname = glob.glob(globalwindatlas_dir + '{}/{}/gwa-plot-data*.zip'.format(country, name))[0]
gwc_fname = glob.glob(globalwindatlas_dir + '{}/{}/gwa3_gwc*.lib'.format(country, name))[0]

era5_level = 133
era5_dir = '/media/liam/drive1/era5/'
year = lambda x: datetime(x, 1, 1)
era5_config = {
    'level133-2008-2009/data/': (year(2008), year(2010), 133, ('u','v'), 484),
    'level133-2010-2019/data/': (year(2010), year(2020), 133, ('u','v','t'), 484),
}

############
### main ###
############

lat = float(name.split(',')[0])
lon = float(name.split(',')[1])

# get ERA5 wind data
era5_reader = era5_mod.Reader(era5_dir, era5_config)
u_wind = era5_reader.read_data(date_start, date_end, era5_level, 'u', lat, lon, interpolated=True)
v_wind = era5_reader.read_data(date_start, date_end, era5_level, 'v', lat, lon, interpolated=True)
wind_orig = (u_wind**2 + v_wind**2)**0.5
time = np.arange(date_start, date_end, timedelta(hours=1))

# fire up adjuster
adjuster = gwa_mod.Adjuster(u_wind, v_wind, time, gwc_fname, plots_fname, roughness, height)
adjuster.auto_adjust()
wind = adjuster.wind

# print adjustment results
print('[*] unadjusted wind:  {:.2f} +/- {:.2f} m/s'.format(wind_orig.mean(), wind_orig.std()))
print('[*] adjusted wind:    {:.2f} +/- {:.2f} m/s'.format(wind.mean(), wind.std()))
adjuster.quanify_adjustment()

