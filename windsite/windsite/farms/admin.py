from django.contrib import admin
from farms import models

admin.site.register(models.Farm)
admin.site.register(models.Trace)
admin.site.register(models.GwaStat)
admin.site.register(models.SeriesEra5Wind)
admin.site.register(models.SeriesNinjaWind)
admin.site.register(models.SeriesEra5CF)
admin.site.register(models.SeriesNinjaCF)
admin.site.register(models.PowerCurve)
admin.site.register(models.ResultNinja)
admin.site.register(models.ResultEra5)