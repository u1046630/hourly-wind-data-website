from django.db import models

from datetime import datetime
import numpy as np

class GwaStat(models.Model):
    # Global Wind Atlas Statistic
    name = models.CharField(max_length=100, unique=True)
    lat = models.FloatField()
    lon = models.FloatField()
    gwc_file = models.FileField(upload_to='GwaStat/gwc_file')
    plots_file = models.FileField(upload_to='GwaStat/plot_file')
    roughness = models.FloatField()
    height = models.FloatField()

    def __str__(self):
        return self.name

class PowerCurve(models.Model):
    name = models.CharField(unique=True, max_length=100)
    power = models.FloatField()
    diameter = models.FloatField()
    source = models.CharField(max_length=100) # 'thewindpower'

    # csv file with two columns: 'wind' and 'power'
    data = models.FileField(upload_to='PowerCurve/data')

    def __str__(self):
        return self.name

###################
### wind speeds ###
###################

class SeriesWind(models.Model):
    lat = models.FloatField()
    lon = models.FloatField()
    height = models.FloatField()
    date_start = models.DateTimeField() # inclusive first hour of trace data
    date_end = models.DateTimeField() # exclusive last hour of trace data

    class Meta:
        abstract = True

class SeriesEra5Wind(SeriesWind):
    # csv file with two columns, 'datetime' and 'wind' (ERA5 wind speed)
    # must have all hours between date_start and date_end
    # wind must be horizontally and vertically interpolated
    data = models.FileField(upload_to='SeriesEra5Wind/data')

class SeriesNinjaWind(SeriesWind):
    # csv file with two columns, 'datetime' and 'wind' (MERRA wind speed from renewables.ninja)
    # must have all hours between date_start and date_end
    # wind must be horizontally and vertically interpolated by Renewables.Ninja
    data = models.FileField(upload_to='SeriesNinjaWind/data')

########################
### capacity factors ###
########################

class SeriesCF(models.Model):
    date_start = models.DateTimeField() # inclusive first hour of trace data
    date_end = models.DateTimeField() # exclusive last hour of trace data
    power_curve = models.ForeignKey(PowerCurve, on_delete=models.CASCADE, null=True)
    # name of a the PowerCurve object used to calculate CFs (may not be same as farm's turbine model)

    class Meta:
        abstract = True

class SeriesEra5CF(SeriesCF):
    wind = models.ForeignKey(SeriesEra5Wind, on_delete=models.CASCADE, null=True)
    wakeloss = models.FloatField()
    sigma = models.FloatField()
    # csv file with two columns, 'datetime' and 'cf_era5'
    # must have all hours between date_start and date_end
    data = models.FileField(upload_to='SeriesEra5CF/data')

class SeriesNinjaCF(SeriesCF):
    wind = models.ForeignKey(SeriesNinjaWind, on_delete=models.CASCADE, null=True)
    alpha = models.FloatField()
    beta = models.FloatField()
    # csv file with two columns, 'datetime' and 'cf_ninja'
    # must have all hours between date_start and date_end
    data = models.FileField(upload_to='SeriesNinjaCF/data')

#############
### other ###
#############

class Farm(models.Model):
    name = models.CharField(unique=True, max_length=100)
    lat = models.FloatField()
    lon = models.FloatField()
    height = models.FloatField()
    num_turbines = models.IntegerField()
    #turbine_density = models.FloatField(null=True) # num per km2
    capacity = models.FloatField() # MW
    buildtype = models.CharField(max_length=100) # 'Offshore', 'Onshore'
    country = models.CharField(max_length=100) # 'uk', 'belgium', 'denmark'
    date_start = models.DateTimeField() # inclusive first hour of traces data
    date_end = models.DateTimeField() # exclusive last hour of traces data
    turbine_model = models.CharField(max_length=100) # the actual turbine, not necessarily the power curve used
    era5_wind = models.ForeignKey(SeriesEra5Wind, null=True, on_delete=models.CASCADE)
    ninja_wind = models.ForeignKey(SeriesNinjaWind, null=True, on_delete=models.CASCADE)
    power_curve = models.ForeignKey(PowerCurve, on_delete=models.CASCADE, null=True) # appropriate power curve to use for analysis

    @property
    def num_traces(self):
        return len(self.trace_set.all())

    def __str__(self):
        return self.name


class Trace(models.Model):
    name = models.CharField(unique=True, max_length=100)
    uid = models.CharField(unique=True, max_length=100)
    source = models.CharField(max_length=100)
    date_start = models.DateTimeField() # inclusive first hour of trace data
    date_end = models.DateTimeField() # exclusive last hour of trace data
    farm = models.ForeignKey(Farm, null=True, on_delete=models.CASCADE) # trace points to one farm

    # csv file with three columns, 'datetime', 'power' and 'cf_trace' 
    # since power to cf is really a preprocessing job
    data = models.FileField(upload_to='Trace/data', null=True)
    plot_file = models.FileField(upload_to='Trace/plot', null=True)

    def __str__(self):
        return self.name


class Result(models.Model):
    description = models.CharField(max_length=100)
        # 'ninja': comparing ninja CF to trace CF
        # 'era5 fixed': comparing ERA5 CF (calculated from fixed param values) to trace CF
        # 'era5 timeseries': comparing ERA5 CF (param values optimising timeseries fit) to trace CF
        # 'fit_dcurve': comparing ERA5 CF (param values optimising duration curve fit) to trace CF
    trace = models.ForeignKey(Trace, null=True, on_delete=models.CASCADE)
    rmse_hourly = models.FloatField()
    rmse_daily = models.FloatField()
    rmse_dcurve = models.FloatField()

    class Meta:
        abstract = True

class ResultNinja(Result):
    
    ninja_cf = models.ForeignKey(SeriesNinjaCF, null=True, on_delete=models.CASCADE)
    plot_file = models.FileField(upload_to='ResultNinja/plot', null=True)

class ResultEra5(Result):
    
    era5_cf = models.ForeignKey(SeriesEra5CF, null=True, on_delete=models.CASCADE)
    plot_file = models.FileField(upload_to='ResultEra5/plot', null=True)