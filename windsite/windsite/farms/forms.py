from django.forms import ModelForm, Form
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import GwaStat, PowerCurve

from datetime import datetime, date

class GwaStatForm(ModelForm):
    class Meta:
        model = GwaStat
        fields = ['name', 'lat', 'lon', 'roughness', 'height', 'gwc_file', 'plots_file']

class DateInput(forms.DateInput):
    input_type = 'date'

class APIForm(Form):
    lat = forms.FloatField(label='Latitude', initial=50.0)
    lon = forms.FloatField(label='Longitude', initial=0.0)
    height = forms.IntegerField(label='Hub height (m)', initial=100, min_value=0, max_value=500)
    custom_date_range = forms.BooleanField(initial=False, required=False)
    year = forms.IntegerField(
        label='Year', 
        initial=2019,
        widget=forms.Select(choices=[(x,x) for x in range(1980,2020)]),
    )
    date_from = forms.DateField(widget=DateInput, initial=date(2010, 1, 1))
    date_to = forms.DateField(widget=DateInput, initial=date(2019, 12, 31))
    period = forms.ChoiceField(
        choices=[('hour', 'hourly'), ('day', 'daily'), ('month', 'monthly')],
        widget=forms.RadioSelect,
        initial='hour',
        required=False,
    )
    calculate_capacity_factors = forms.BooleanField(initial=True, required=False)
    turbine = forms.ModelChoiceField(
        label='Wind turbine', 
        queryset=PowerCurve.objects.all().order_by('name'), 
        initial=PowerCurve.objects.get(name='Vestas V126-3450'),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(APIForm, self).__init__(*args, **kwargs)
        self.fields['calculate_capacity_factors'].widget.attrs['onclick'] = "javascript:updateForm();"
        self.fields['custom_date_range'].widget.attrs['onclick'] = "javascript:updateForm();"
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('lat', css_class='form-group col-md-4 mb-0'),
                Column('lon', css_class='form-group col-md-4 mb-0'),
                Column('height', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            'custom_date_range',
            Row(
                Column('year', css_class='form-group col-md-4 mb-0'),
                id='row_id_year_selector',
                css_class='form-row'
            ),
            Row(
                Column('date_from', css_class='form-group col-md-4 mb-0'),
                Column('date_to', css_class='form-group col-md-4 mb-0'),
                id='row_id_custom_date_selector',
                css_class='form-row',
            ),
            'calculate_capacity_factors',
            'turbine', 
            'period',
            Submit('download', 'Download Data'),
            Submit('api-request', 'View API Request'),
        )

    def clean_date_to(value):
        date_to = datetime.strptime(value.data['date_to'], '%Y-%m-%d').date()
        if 'custom_date_range' in value.data:
            if date_to < date(1980,1,1) or date_to >= date(2020,1,1):
                raise forms.ValidationError('Must be in range 1980 to 2019')
            date_from = datetime.strptime(value.data['date_from'], '%Y-%m-%d').date()
            if date_to < date_from:
                raise forms.ValidationError('Must be greater than "Date from"')
        return date_to

    def clean_date_from(value):
        date_from = datetime.strptime(value.data['date_from'], '%Y-%m-%d').date()
        if 'custom_date_range' in value.data:
            if date_from < date(1980,1,1) or date_from >= date(2020,1,1):
                raise forms.ValidationError('Must be in range 1980 to 2019')
        return date_from

    def clean_lat(value):
        lat = float(value.data['lat'])
        if lat < -90 or lat > 90:
            raise forms.ValidationError('Must be in range -90 to 90')
        return lat

    def clean_lon(value):
        lon = float(value.data['lon'])
        if lon < -180 or lon > 180:
            raise forms.ValidationError('Must be in range -180 to 180')
        return lon

    def clean_height(value):
        height = int(value.data['height'])
        if height < 0 or height > 500:
            raise forms.ValidationError('Must be in range 0 to 500')
        return height