from django.urls import path
from .views import *

urlpatterns = [
    path('', MapView.as_view(), name='farms-map'),
    path('about/', AboutPageView.as_view(), name='farms-about'),
    path('farm/', FarmView.as_view(), name='farms-farm'),
    path('farms/', FarmsView.as_view(), name='farms-farms'),
    path('trace/', TraceView.as_view(), name='farms-trace'),
    path('gwa-stat/', GwaStatView.as_view(), name='farms-gwa-stat'),
    path('gwa-stats/', GwaStatsView.as_view(), name='farms-gwa-stats'),
    path('gwa-form/', GwaStatFormView.as_view(), name='farms-gwa-form'),
    path('json/farms/', FarmsJsonView.as_view(), name='json-farms'),
    path('download/', CoordView.as_view(), name='farms-download'),
    path('docs/data/', InfoPageView.as_view(), name='farms-docs-data'),
    path('docs/api/', ApiDocsView.as_view(), name='farms-docs-api'),
    path('turbine/', TurbineView.as_view(), name='farms-turbine'),
    path('turbines/', TurbinesView.as_view(), name='farms-turbines'),
    path('supplementary/', SupplementaryView.as_view(), name='farms-supplementary-data')
]
