var mymap = L.map('map_div').setView([20, 30], 3);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 12,
    minZoom: 3,
    id: 'liamhayes/ckc3zouez07h21imysfoqgwe1',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibGlhbWhheWVzIiwiYSI6ImNrYzQwM3gyNTAzMzYyeG82emRoamV6Y2oifQ._x7rlu2t2XofurdReKYE6g',
}).addTo(mymap);

var poly_style = {fillColor: '#000000', color: '#000000'}

var s = 32
var farmIcon = L.icon({
    iconUrl: url_icon,

    iconSize:     [s, s], // size of the icon
    iconAnchor:   [s/2, s], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, -s] // point from which the popup should open relative to the iconAnchor
});

$.getJSON(url_jsonfarms, function(test) { 
    for (f of test['farms']) {
        L.marker([f['lat'], f['lon']], {icon: farmIcon}).addTo(mymap).bindPopup('<a href="' + f['link'] + '">' + f['name'] + '</a>');
    }
    
});

var popup = L.popup();

function onMapClick(e) {
    var lat = e.latlng.lat.toFixed(2);
    var lon = e.latlng.lng.toFixed(2);
    popup
        .setLatLng(e.latlng)
        .setContent(`${lat}, ${lon}<br><a href="${url_coord}?lat=${lat}&lon=${lon}">Download data</a>`)
        .openOn(mymap);
}

mymap.on('click', onMapClick);