
# Instructions on how to calculate which N320 points are "offshore".

1. Download Global Wind Atlas worldwide wind speeds (GWA > Download GIS Files > the world > wind speed > 100m). File is 12GB TIF, takes a while.
2. Import TIF into QGIS, generate another raster layer defined on the same pixels that is 0 everywhere, this makes the next step much much quicker (Raster Calculater > layer*0)
3. Polygonize new raster layer, this will draw a polygon boundary around the raster layer (raster > conversion > polygonize). 
4. Polygonize step produces a 16MB file. Save this file for future use (`gwa-boundary-polygon.shp`).
5. Download `countries.shp` from somewhere online, fix geometries in QGIS if needed (processing > processing toolbox > fix...)
6. Make shapefile containing all of the N320 grid points (`scripts/manage_era5/create_n320_shapefile.py`), called `points-all.shp`
7. Import `point-all.shp`, `gwa-boundary.shp` and `countries.shp` into fresh QGIS project. 
8. Clip `points-all` using `gwa-boundary-polygon` to make `points-gwa`. Clip `points-gwa` using `countries` to make `points-onshore` layer. Difference `points-gwa` with `points-onshore` to make `points-offshore`. This is so so much quicker than trying to difference `points-gwa` with `countries`.

The resulting files (`points-onshore.shp` and `points-offshore.shp`) on represent areas that are covered by the Global Wind Atlas, so Antartica, Artic and ocean more than 200km from coastline is not included.
