'''
save GwaStat objects in django database to a zipfile.
since users can create GwaStat objects on the website,
this is useful when a website rebuild is required without data loss.
'''

from farms.models import GwaStat

import os
from datetime import datetime
from zipfile import ZipFile

output_zip = datetime.strftime(datetime.now(), 'datadumps/%Y-%m-%d_%H:%M:%S.zip')

print('[*] dumping GwaStats: ', flush=True)

with ZipFile(output_zip, 'w') as z:

    for gwa_stat in GwaStat.objects.all():
        name = gwa_stat.name
        
        info = '\n'.join([
            f'name:{name}',
            f'lat:{gwa_stat.lat}',
            f'lon:{gwa_stat.lon}',
            f'roughness:{gwa_stat.roughness}',
            f'height:{gwa_stat.height}',
        ])
        with z.open(os.path.join(name, 'info.txt'), 'w') as f:
            f.write(bytes(info, encoding='utf-8'))
        with z.open(os.path.join(name, 'plots_file.zip'), 'w') as f:
            f.write(gwa_stat.plots_file.file.read())
        with z.open(os.path.join(name, 'gwc_file.lib'), 'w') as f:
            f.write(gwa_stat.gwc_file.file.read())