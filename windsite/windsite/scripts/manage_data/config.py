CAPACITY_WINDOW_SIZE = 24 * 20 # estimate capacity based smallest maximum power generated in any 20 day window that includes a given datetime. 

turbine_to_power_curve = {
    'Adwen AD 5-135': 'Vestas V112-3450',
    'Siemens SWT-3.0-108': 'Vestas V100-2600',
    'Siemens SWT-4.0-130': 'Vestas V112-3000 Offshore',
    'Siemens SWT-6.0-154': 'Vestas V117-3450',
    'Siemens SWT-6.0-120': 'Vestas V66-1750',
    'Siemens SWT-7.0-154': 'Siemens SWT-3.0-101',
    'Vestas V90-3000 Offshore': 'Vestas V90-3000',
    'Bard VM': 'Siemens SWT-2.3-82',
    'Siemens SWT-4.0-120': 'Vestas V112-3450', 
    'GE Energy Haliade 150': 'Vestas V112-3300',
    'MHI Vestas Offshore V164-8400': 'Vestas V80-2000',
    'Siemens-Gamesa SG 8.0-167 DD': 'Gamesa G132-5000', 
}

# turbine_to_power_curve_ninja = {
#     'Bonus B82-2300': 'Bonus B82 2300',
#     'MHI Vestas Offshore V112-3300': 'Vestas V112 3300',
#     'MHI Vestas Offshore V164-8000': 'Vestas V164 8000',
#     'MHI Vestas Offshore V164-8400': 'Vestas V80 2000', # same alternative
#     'Repower 5M': 'REpower 5M',
#     'Senvion 6M126': 'Vestas V66 1650', # different alternative
#     'Siemens-Gamesa SG 8.0-167 DD': 'Vestas V117 4000', # different alternative
#     'Siemens SWT-2.3-93': 'Siemens SWT 2.3 93',
#     'Siemens SWT-3.2-113': 'Siemens SWT 3.6 120', # different alternative
#     'Siemens SWT-3.6-107': 'Siemens SWT 3.6 107',
#     'Siemens SWT-3.6-120': 'Siemens SWT 3.6 120',
#     'Siemens SWT-6.0-154': 'Siemens SWT 3.6 120', # different alternative
#     'Siemens SWT-7.0-154': 'Siemens SWT 3.0 101', # same alternative
#     'Vestas V112-3000 Offshore': 'Vestas V112 3000',
#     'Vestas V112-3450': 'Vestas V80 1800', # different alternative
#     'Vestas V164-8000': 'Vestas V164 8000',
#     'Vestas V80-2000': 'Vestas V80 2000',
#     'Vestas V90-3000 Offshore': 'Vestas V90 3000',
# }

thewindpower_corrections = {
    # uk
    'Beatrice': {'Hub height (m)': 101},
    'Dudgeon': {'Hub height (m)': 110},
    'Galloper': {'Hub height (m)': 103.5},
    'Walney Extension - Part 1': {'Hub height (m)': 105},
    'Walney Extension - Part 2': {'Hub height (m)': 105},
    'Hornsea Project One - Njord': {'Commissioning': '2019/08', 'Hub height (m)': 100}, # hub height guess
    'Hornsea Project Two - Breesea and Optimus Wind': {'Hub height (m)': 150}, #hub height guess
    'Hywind Scotland Pilot Park': {'Turbine model': 'Siemens SWT-3.2-113'},
    'Walney - Part 1': {'Latitude': 54.081, 'Longitude': -3.60497222222222},
    'Rampion': {'Latitude': 50.660734, 'Longitude': -0.278749},

    # belgium
    'Norther': {'Hub height (m)': 105}, 

    # germany
    #'Wikinger Offshore': {'Hub height (m)': 75, 'Commissioning': '2018/02'},
    #'Borkum Riffgrund II': {'Hub height (m)': 105},
    #'EnBW Hohe See': {'Hub height (m)': 105},
    #'EnBW Baltic 1': {'Power (kW)': 20/21*48300}, # only 20 of 21 turbines
    #'Arkona': {'Commissioning': '2018/11'},

    # netherlands
    #'Gemini': {'Commissioning': '2016/06'},  
}

# maps name of 4coffshore wind farm to the multiple
# wind farms it represents
coffshore_farms = {
    # uk
    'Barrow': ['Barrow'], 
    'Beatrice': ['Beatrice'], 
    'Burbo Bank': ['Burbo Bank - Part 1'], 
    'Burbo Bank Extension': ['Burbo Bank - Part 2'], 
    'Dudgeon': ['Dudgeon'], 
    'Aberdeen Offshore Wind Farm (EOWDC)': ['EOWDC - Part 1', 'EOWDC - Part 2'], 
    'Galloper': ['Galloper'], 
    'Greater Gabbard': ['Greater Gabbard 1', 'Greater Gabbard 2'], 
    'Gunfleet Sands': ['Gunfleet Sands 1', 'Gunfleet Sands 2'], 
    'Gwynt y Môr': ['Gwynt y Môr'], 
    'Hornsea Project One': ['Hornsea Project One - Njord', 'Hornsea Project One - Heron Wind'], 
    'Hornsea Project Two': ['Hornsea Project Two - Breesea and Optimus Wind'], 
    'Humber Gateway': ['Humber Gateway'], 
    'Hywind Scotland Pilot Park': ['Hywind Scotland Pilot Park'], 
    'Lincs': ['Lincs'], 
    'London Array': ['London Array'], 
    'Ormonde': ['Ormonde'], 
    'Race Bank': ['Race Bank'], 
    'Rampion': ['Rampion'], 
    'Robin Rigg': ['Robin Rigg'], 
    'Walney Phase 1': ['Walney - Part 1'], 
    'Walney Phase 2': ['Walney - Part 2'], 
    'Walney Extension': ['Walney Extension - Part 1', 'Walney Extension - Part 2'], 
    'West of Duddon Sands': ['West of Duddon Sands'], 

    # denmark
    'Anholt': ['Anholt'], 
    'Horns Rev 1': ['Horns Rev 1'], 
    'Horns Rev 2': ['Horns Rev 2'], 
    'Horns Rev 3': ['Horns Rev 3'], 
    'Rødsand 2': ['Rodsand II'], 
    'Nysted': ['Nysted Offshore'], 

    # belgium
    'Belwind': ['Belwind I'], 
    'Nobelwind': ['Belwind II'], 
    'Norther': ['Norther'], 
    'Northwind': ['Northwind'], 
    'Rentel': ['Rentel'],
    'Thornton Bank phase II': ['Thorntonbank - Part 2'], 
    'Thornton Bank phase III': ['Thorntonbank - Part 3'], 
}

coffshore_countries = {
    'United Kingdom': 'uk',
    'Denmark': 'denmark',
    'Belgium': 'belgium',
}

entsoe_traces = {
                
    ########
    ## UK ##
    ########
    
    'Robin Rigg 1': {
        'country': 'uk',
        'code': '48W000000RREW-14',
        'farm': 'Robin Rigg', 
    },
    'Robin Rigg 2': {
        'country': 'uk',
        'code': '48W000000RRWW-1P',
        'farm': 'Robin Rigg',
    },
    'EOWDC': { # aka: Aberdeen Offshore Wind Farm
        'country': 'uk',
        'code': '48W00000ABRBO-19',
        'farm': 'EOWDC - Part 1', 
    },
    'Beatrice 1': {
        'country': 'uk',
        'code': '48W00000BEATO-1T',
        'farm': 'Beatrice',
    },
    'Beatrice 2': {
        'country': 'uk',
        'code': '48W00000BEATO-2R',
        'farm': 'Beatrice',
    },
    'Beatrice 3': {
        'country': 'uk',
        'code': '48W00000BEATO-3P',
        'farm': 'Beatrice',
    },
    'Beatrice 4': {
        'country': 'uk',
        'code': '48W00000BEATO-4N',
        'farm': 'Beatrice',
    },
    'Barrow': {
        'country': 'uk',
        'code': '48W00000BOWLW-1K',
        'farm': 'Barrow',
    },
    'Burbo Bank 1': {
        'country': 'uk',
        'code': '48W00000BURBW-1L',
        'farm': 'Burbo Bank - Part 1',
    },
    'Burbo Bank 2': {
        'country': 'uk',
        'code': '48W00000BRBEO-17',
        'farm': 'Burbo Bank - Part 2',
    },
    'Dudgeon 1': {
        'country': 'uk',
        'code': '48W00000DDGNO-1E',
        'farm': 'Dudgeon',
    },
    'Dudgeon 2': {
        'country': 'uk',
        'code': '48W00000DDGNO-2C',
        'farm': 'Dudgeon',
    },
    'Dudgeon 3': {
        'country': 'uk',
        'code': '48W00000DDGNO-3A',
        'farm': 'Dudgeon',
    },
    'Dudgeon 4': {
        'country': 'uk',
        'code': '48W00000DDGNO-48',
        'farm': 'Dudgeon',
    },
    'Gunfleet Sands 1': {
        'country': 'uk',
        'code': '48W00000GNFSW-1H',
        'farm': 'Gunfleet Sands 1',
    },
    'Gunfleet Sands 2': {
        'country': 'uk',
        'code': '48W00000GNFSW-2F',
        'farm': 'Gunfleet Sands 2',
    },
    'Greater Gabbard 1': {
        'country': 'uk',
        'code': '48W00000GRGBW-1V',
        'farm': 'Greater Gabbard 1',
    },
    'Greater Gabbard 2': {
        'country': 'uk',
        'code': '48W00000GRGBW-2T',
        'farm': 'Greater Gabbard 2',
    },
    'Galloper': { # aka Greater Gabbard Extension
        'country': 'uk',
        'code': '48W00000GRGBW-3R',
        'farm': 'Galloper',
    },
    'Humber Gateway': {
        'country': 'uk',
        'code': '48W00000HMGTO-10',
        'farm': 'Humber Gateway',
    },
    'Hornsea Project One': {
        'country': 'uk',
        'code': '48W00000HOWAO-2K',
        'farm': 'Hornsea Project One - Njord',
    },
    'Hornsea Project Two': {
        'country': 'uk',
        'code': '48W00000HOWAO-3I',
        'farm': 'Hornsea Project Two - Breesea and Optimus Wind',
    },
    'Hywind': {
        'country': 'uk',
        'code': '48W00000HYWDW-1G',
        'farm': 'Hywind Scotland Pilot Park',
    },
    'London Array 1': {
        'country': 'uk',
        'code': '48W00000LARYO-1Z',
        'farm': 'London Array',
    },
    'London Array 2': {
        'country': 'uk',
        'code': '48W00000LARYO-2X',
        'farm': 'London Array',
    },
    'London Array 3': {
        'country': 'uk',
        'code': '48W00000LARYO-3V',
        'farm': 'London Array',
    },
    'London Array 4': {
        'country': 'uk',
        'code': '48W00000LARYO-4T',
        'farm': 'London Array',
    },
    'Lincs 1': {
        'country': 'uk',
        'code': '48W00000LNCSO-1R',
        'farm': 'Lincs',
    },
    'Lincs 2': {
        'country': 'uk',
        'code': '48W00000LNCSO-2P',
        'farm': 'Lincs',
    },
    'Ormonde': {
        'country': 'uk',
        'code': '48W00000OMNDO-1J',
        'farm': 'Ormonde',
    },
    'Race Bank 1': {
        'country': 'uk',
        'code': '48W00000RCBKO-1S',
        'farm': 'Race Bank',
    },
    'Race Bank 2': {
        'country': 'uk',
        'code': '48W00000RCBKO-2Q',
        'farm': 'Race Bank',
    },
    'Rampion 1': {
        'country': 'uk',
        'code': '48W00000RMPNO-17',
        'farm': 'Rampion',
    },
    'Rampion 2': {
        'country': 'uk',
        'code': '48W00000RMPNO-25',
        'farm': 'Rampion',
    },
    'West of Duddon Sands 1': {
        'country': 'uk',
        'code': '48W00000WDNSO-1H',
        'farm': 'West of Duddon Sands',
    },
    'West of Duddon Sands 2': {
        'country': 'uk',
        'code': '48W00000WDNSO-2F',
        'farm': 'West of Duddon Sands', 
    },
    'Walney 1': {
        'country': 'uk',
        'code': '48W00000WLNYW-1A',
        'farm': 'Walney - Part 1',
    },
    'Walney 2': {
        'country': 'uk',
        'code': '48W00000WLNYO-23',
        'farm': 'Walney - Part 2',
    },
    'Walney Extension 1': {
        'country': 'uk',
        'code': '48W00000WLNYO-31',
        'farm': 'Walney Extension - Part 1',
    },
    'Walney Extension 2': {
        'country': 'uk',
        'code': '48W00000WLNYO-4-',
        'farm': 'Walney Extension - Part 2',
    },
    'Gwynt y Mor 1': {
        'country': 'uk',
        'code': '48W0000GYMRO-15O',
        'farm': 'Gwynt y Môr',
    },
    'Gwynt y Mor 2': {
        'country': 'uk',
        'code': '48W0000GYMRO-17K',
        'farm': 'Gwynt y Môr',
    },
    'Gwynt y Mor 3': {
        'country': 'uk',
        'code': '48W0000GYMRO-26J',
        'farm': 'Gwynt y Môr',
    },
    'Gwynt y Mor 4': {
        'country': 'uk',
        'code': '48W0000GYMRO-28F',
        'farm': 'Gwynt y Môr',
    },

    #############
    ## Belgium ##
    #############
    
    'Belwind II': { # aka Nobel Wind
        'country': 'belgium',
        'code': '22W20161115----Z',
        'farm': 'Belwind II', 
    },
    'Rentel': {
        'country': 'belgium',
        'code': '22W20180615----H',
        'farm': 'Rentel',
    },
    'Norther': {
        'country': 'belgium',
        'code': '22W201902151---T',
        'farm': 'Norther',
    },
    'Belwind I': {
        'country': 'belgium',
        'code': '22WBELWIN1500271',
        'farm': 'Belwind I',
    },
    'Northwind': {
        'country': 'belgium',
        'code': '22WNORTHW150187B',
        'farm': 'Northwind',
    },
    'Thorntonbank - Part 3': {
        'country': 'belgium',
        'code': '22WTHORNT150237E',
        'farm': 'Thorntonbank - Part 3',
    },
    'Thorntonbank - Part 2': {
        'country': 'belgium',
        'code': '22WTHORNT150238C',
        'farm': 'Thorntonbank - Part 2',
    },
                
    #############
    ## Denmark ##
    #############
    
    'Anholt': {
        'country': 'denmark',
        'code': '45W000000000046I',
        'farm': 'Anholt',
    },
    'Horns Rev 1': {
        'country': 'denmark',
        'code': '45W000000000047G',

        'farm': 'Horns Rev 1',
    },
    'Horns Rev 2': {
        'country': 'denmark',
        'code': '45W000000000048E',
        'farm': 'Horns Rev 2',
    },
    'Horns Rev 3': {
        'country': 'denmark',
        'code': '45W000000000116N',
        'farm': 'Horns Rev 3',
    },
    'Nysted Offshore': { # aka: 'Rodsand I'
        'country': 'denmark',
        'code': '45W000000000044M',
        'farm': 'Nysted Offshore', 
    },    
    'Rodsand II': {
        'country': 'denmark',
        'code': '45W000000000045K',
        'farm': 'Rodsand II',
    },
}