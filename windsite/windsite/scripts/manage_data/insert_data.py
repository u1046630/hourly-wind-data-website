'''
todo:
    - do more checks to make sure the Ninja datalet correctly corresponds to the farm object
    - eventually make the ERA5 datalets sourced from the API (to absolutely make sure it is as advertised)
'''

import io, shutil, os, progressbar, glob
from zipfile import ZipFile
import numpy as np, pandas as pd
from datetime import datetime, timedelta
import boto3, era5_mod

from django.core.files import File
from django.conf import settings

from farms.models import GwaStat, PowerCurve, Farm, Trace, ResultNinja, ResultEra5
from farms.models import SeriesEra5Wind, SeriesNinjaWind, SeriesEra5CF, SeriesNinjaCF
from scripts.manage_data import config

#from scripts.manage_era5.local_reader import get_wind_speed

###############
### globals ###
###############

# globals
ninja_dir = '/media/liam/drive1/ninja/data/'
entsoe_dir = '/media/liam/drive1/entsoe/processed/ActualGenerationOutputPerUnit/'
thewindpower_dir = '/media/liam/drive1/thewindpower/'
coffshore_fname = '/media/liam/drive1/4coffshore/windfarms-data.csv'
gwastat_dump = '/media/liam/drive1/globalwindatlas/website-backup/chengs-uploads-2020Aug19.zip'

coffshore_data = pd.read_csv(coffshore_fname, encoding='utf-16').set_index('name')

s3 = boto3.resource("s3")
bucket = s3.Bucket('windsite-files')
era5_data_dir = bucket
date = lambda x: datetime(x, 1, 1)
era5_config = {
    'era5/1980-1989/': (date(1980), date(1990)),
    'era5/1990-1999/': (date(1990), date(2000)),
    'era5/2000-2009/': (date(2000), date(2010)),
    'era5/2010-2019/': (date(2010), date(2020)),
}
era5_reader = era5_mod.NewReader(era5_data_dir, era5_config)

#############
### funcs ###
#############

def power_to_cf(power):
    '''guess the timeseries capacity of a trace from the power,
    and calculate the timeseries capacity factor'''
    hour = timedelta(hours=1)
    power = power.reindex(np.arange(power.index[0], power.index[-1], hour), fill_value=0.0)
    capacity = power.rolling(config.CAPACITY_WINDOW_SIZE, center=True, min_periods=1).max() # max power in closest 20 days
    cf = power / capacity
    return cf

def delete_all():
    delete(Trace)
    delete(Farm)
    delete(SeriesEra5Wind)
    delete(SeriesNinjaCF)
    delete(PowerCurve)
    delete(GwaStat)
    if os.path.isdir(settings.MEDIA_ROOT):
        shutil.rmtree(settings.MEDIA_ROOT)

def delete(model):
    print(f'[*] deleting {model}...')
    for obj in model.objects.all():
        obj.delete()

def insert():

    print('[*] inserting power curves:')
    pcurves = pd.read_csv(thewindpower_dir + 'turbines/turbine-data.csv').set_index('name')
    for pcurve_name, pcurve in progressbar.progressbar(list(pcurves.iterrows())):
        if pcurve['power curve'] == False:
            continue
        fname = thewindpower_dir + f'turbines/power-curve-data/{pcurve.name}.csv'
        pcurve_data = pd.read_csv(fname).rename(columns={'Wind speed (m/s)': 'wind', 'Power (kW)': 'power'}).set_index('wind')
        buf = io.StringIO()
        pcurve_data.to_csv(buf)
        pcurve_obj = PowerCurve(
            name=pcurve.name,
            power=pcurve.power,
            diameter=pcurve.diameter,
            source='thewindpower',
            data=File(buf, pcurve.name + '.csv')
        )
        pcurve_obj.save()

    print('[*] inserting GwaStats: ', flush=True)
    with ZipFile(gwastat_dump, 'r') as z:
        for gwa_name in set(x.split('/')[0] for x in z.namelist()):
            with z.open(os.path.join(gwa_name, 'info.txt'), 'r') as f:
                lines = str(f.read(), encoding='utf-8').split('\n')
                params = {x.split(':')[0].strip(): x.split(':')[1].strip() for x in lines}
            with z.open(os.path.join(gwa_name, 'plots_file.zip'), 'r') as f1, \
                    z.open(os.path.join(gwa_name, 'gwc_file.lib'), 'r') as f2:
                gwa_obj = GwaStat(
                    name=params['name'],
                    lat=float(params['lat']),
                    lon=float(params['lon']),
                    roughness=float(params['roughness']),
                    height=float(params['height']),
                    plots_file=File(f1, params['name'] + '.zip'),
                    gwc_file=File(f2, params['name'] + '.lib'),
                )
                gwa_obj.save()

    print('[*] inserting traces and farms: ', flush=True)
    for trace_name, trace in progressbar.progressbar(config.entsoe_traces.items()):

        ### trace ###

        fname_entsoe = entsoe_dir + trace['code'] + '.npz'
        npz = np.load(fname_entsoe)
        data_entsoe = pd.DataFrame({'datetime': npz['DateTime'], 'power': npz['ActualGenerationOutput']})
        data_entsoe = data_entsoe.set_index('datetime').sort_index()
        data_entsoe['cf_trace'] = power_to_cf(data_entsoe.power)
        data_entsoe.dropna(how='any', inplace=True)
        buf = io.StringIO()
        data_entsoe.to_csv(buf)
        buf.seek(0)
        trace_obj = Trace(
            name=trace_name,
            source='entsoe',
            uid=trace['code'],
            date_start=data_entsoe.index.min(),
            date_end=data_entsoe.index.max(),
            data=File(buf, trace['code'] + '.csv'),
            farm=None,
        )

        ### farm ###

        farm_name = trace['farm']
        temp = Farm.objects.filter(name=farm_name)
        if len(temp) == 1:
            farm_obj = temp[0]
            farm_obj.date_start = min(farm_obj.date_start, data_entsoe.index.min())
            farm_obj.date_end = max(farm_obj.date_end, data_entsoe.index.max())
        
        else:
        
            assert(len(temp) == 0)
            fname = thewindpower_dir + f'countries/{trace["country"]}/wind-farm-data.csv'
            farms = pd.read_csv(fname).set_index('Name')

            corrections = config.thewindpower_corrections.get(farm_name, {})
            for k,v in corrections.items():
                farms.loc[farm_name,k] = v

            farm = farms.loc[farm_name] # row from .csv file

            turbine_model = farm['Turbine model']
            power_curve_name = config.turbine_to_power_curve.get(turbine_model, turbine_model)
            power_curve_obj = PowerCurve.objects.get(name=power_curve_name)
            farm_obj = Farm(
                name=farm_name,
                lat=farm['Latitude'],
                lon=farm['Longitude'],
                height=farm['Hub height (m)'],
                num_turbines=farm['Number turbines'],
                capacity=farm['Power (kW)']/1000,
                buildtype=farm['Type'],
                country=trace["country"],
                date_start=data_entsoe.index.min(),
                date_end=data_entsoe.index.max(),
                turbine_model=turbine_model,
                power_curve=power_curve_obj,
            )

        trace_obj.farm = farm_obj
        farm_obj.save()
        trace_obj.save()

    # print('[*] calculating turbine densities: ', flush=True)
    # for coffshore_farm_name, farm_names in config.coffshore_farms.items():
    #     coffshore_farm = coffshore_data.loc[coffshore_farm_name] # df row
    #     country = config.coffshore_countries[coffshore_farm.country]
    #     farms = pd.read_csv(thewindpower_dir + f'countries/{country}/wind-farm-data.csv').set_index('Name')

    #     num_turbines = 0
    #     for farm_name in farm_names:
    #         num_turbines += farms.loc[farm_name]['Number turbines']
    #     density = num_turbines / coffshore_farm.area
    #     for farm_name in farm_names:
    #         query = Farm.objects.filter(name=farm_name)
    #         if len(query) == 1:
    #             farm = query[0]
    #             farm.turbine_density = density
    #             farm.save()

    print('[*] inserting datalets: ', flush=True)
    for farm_obj in progressbar.progressbar(Farm.objects.all()):

        ### ninja datalet ###

        # TODO: check that the stored ninja data has enough datetime coverage,
        # TODO: and that the lat/lon/power_curve/height matches the farm_obj

        fname = ninja_dir + farm_obj.name + '.csv'
        # TODO: first row not being read, wind_speed bad bad
        data_ninja = pd.read_csv(fname, parse_dates=True, index_col=0)
        data_ninja.index.name = 'datetime'
        data_ninja = data_ninja[['wind_speed']].rename(columns={'wind_speed': 'wind'})
        
        buf = io.StringIO()
        data_ninja.to_csv(buf)
        buf.seek(0)
        ninja_obj = SeriesNinjaWind(
            lat=farm_obj.lat,
            lon=farm_obj.lon,
            height=farm_obj.height,
            date_start=data_ninja.index.min(),
            date_end=data_ninja.index.max(),
            data=File(buf, farm_obj.name + '.csv')
        )
        ninja_obj.save()
        farm_obj.ninja_wind = ninja_obj

        ### era5 datalet ###

        lat, lon, height = farm_obj.lat, farm_obj.lon, farm_obj.height
        date_start = datetime(2014, 1, 1)
        date_end = datetime(2020, 1, 1)
        wind_vect = era5_reader.wind_speed(date_start, date_end, height, (lat, lon)) 
        time_vect = np.arange(date_start, date_end, timedelta(hours=1))
        data_era5 = pd.DataFrame({'datetime': time_vect, 'wind': wind_vect}).set_index('datetime')
        buf = io.StringIO()
        data_era5.to_csv(buf)
        buf.seek(0)
        era5_obj = SeriesEra5Wind(
            lat=lat,
            lon=lon,
            height=height,
            date_start=date_start,
            date_end=date_end,
            data=File(buf, farm_obj.name + '.csv')
        )

        era5_obj.save()
        farm_obj.era5_wind = era5_obj

        farm_obj.save()

    # print('[*] inserting power curves:')
    # for farm_obj in progressbar.progressbar(Farm.objects.all()):
    #     pcurves = pd.read_csv(thewindpower_dir + 'turbines/turbine-data.csv').set_index('name')
    #     pcurve = pcurves.loc[farm_obj.power_curve_era5] # row from .csv file

    #     temp = PowerCurve.objects.filter(name=pcurve.name)
    #     if len(temp) == 1:
    #         continue

    #     ### power curve ###
        
    #     fname = thewindpower_dir + f'turbines/power-curve-data/{pcurve.name}.csv'
    #     pcurve_data = pd.read_csv(fname).rename(columns={'Wind speed (m/s)': 'wind', 'Power (kW)': 'power'}).set_index('wind')
    #     buf = io.StringIO()
    #     pcurve_data.to_csv(buf)
    #     pcurve_obj = PowerCurve(
    #         name=pcurve.name,
    #         power=pcurve.power,
    #         diameter=pcurve.diameter,
    #         source='thewindpower',
    #         data=File(buf, pcurve.name + '.csv')
    #     )
    #     pcurve_obj.save()

############
### main ###
############

if __name__ == '__main__':
    delete_all()
    insert()
