from farms.models import SeriesNinjaCF, SeriesEra5CF, Trace
import shutil, os

print('[*] deleting ERA5 result objects...')
if os.path.isdir('media/SeriesEra5CF'):
    shutil.rmtree('media/SeriesEra5CF')
SeriesEra5CF.objects.all().delete()

print('[*] deleting Ninja result objects...')
if os.path.isdir('media/SeriesNinjaCF'):
    shutil.rmtree('media/SeriesNinjaCF')
SeriesNinjaCF.objects.all().delete()

print('[*] deleting plots from Traces...')
if os.path.isdir('media/Trace/plot'):
    shutil.rmtree('media/Trace/plot')
for trace in Trace.objects.all():
    trace.plot_file = None
    trace.save()
