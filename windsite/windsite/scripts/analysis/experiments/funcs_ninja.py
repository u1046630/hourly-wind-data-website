from scipy.interpolate import CubicSpline
import pandas as pd, numpy as np

from scripts.analysis.config import ninja_adjustments

def gaussian_filter(x, X, sigma):
    n = X - x
    return np.exp(-n**2 / (2*sigma)) / np.sqrt(2 * np.pi * sigma)

def smooth_pcurve(pcurve):
    # as directed in supplementary material of Staffel paper
    # section 4.3
    
    spline = CubicSpline(pcurve['wind'], pcurve['power'])
    res = 0.01
    X = np.arange(0, 40, res) # pts where pcurve is to be defined
    Y = spline(X) # cubic spline of power curve to higher resolution
    Y_prime = np.zeros(Y.shape)
    N = len(X)
    assert(len(Y) == N)

    for i in range(N):
        x = X[i]
        sigma = 0.6 + 0.2*x # sigma as function of wind speed formula
        #sigma = (0.6 + 0.2*x)*2.0 # fudge factor, the website seems to use twice the sigma
        g_filter = gaussian_filter(x, X, sigma)
        Y_prime[i] = np.sum(Y * g_filter)*res
        
    return pd.DataFrame({'wind': X, 'power': Y_prime})

# WARNING: wind speeds downloaded from renewables.ninja are already bias corrected
# (if location is based in Europe), this this bias correction step is not needed
def ninja_adjust_wind(wind, country):
    if country not in ninja_adjustments:
        raise Exception('[!] No Ninja adjustment parameters found for country "{country}"')
    alpha = ninja_adjustments[country]['alpha_offshore']
    #alpha = ninja_adjustments[country]['alpha']
    beta = ninja_adjustments[country]['beta']
    return wind * alpha + beta
    
def ninja_wind_to_cf(wind, country, pcurve):
    '''predict capacity factor from wind and power curve, according to renewables ninja method'''
    
    # adjust wind:
    #wind = ninja_adjust_wind(wind, country) # see WARNING
    # normalise pcurve:
    pcurve.power = pcurve.power / pcurve.power.max()
    # gaussian smooth pcurve:
    pcurve = smooth_pcurve(pcurve)
    # interpolate to get capacity factors
    return np.interp(wind, pcurve.wind, pcurve.power)