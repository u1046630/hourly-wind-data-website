
import pandas as pd, numpy as np, os
import progressbar
from .funcs import *
from farms.models import Trace, Farm, PowerCurve, SeriesNinjaCF
from scripts.analysis.experiments.base import Experiment

from scripts.analysis.experiments.funcs_ninja import ninja_wind_to_cf, ninja_adjustments

from django.conf import settings

class Ninja(Experiment):

    def __init__(self):
        self.exp_name = 'ninja'
        super().__init__()

    def run(self):
        results = []
        traces = Trace.objects.all()

        for trace in progressbar.progressbar(traces):
            data_trace = read_timeseries_csv(trace.data.file)
            data_ninja = read_timeseries_csv(trace.farm.ninja_wind.data.file)
            data = data_trace.join([data_ninja], how='inner')
            data = data.sort_index()
            
            pcurve = read_pcurve(trace.farm.power_curve.data.file)
            country = trace.farm.country
            data['cf_ninja'] = ninja_wind_to_cf(data.wind, country, pcurve)

            result = {
                'trace': trace.name,
                'alpha': ninja_adjustments[country]['alpha_offshore'],
                'beta': ninja_adjustments[country]['beta'],
                'wind_mean': np.mean(data.wind),
                'wind_std': np.std(data.wind),
                'cf_mean': np.mean(data.cf_ninja),
            }
            result.update(calc_ninja_errors(data))
            results.append(result)

            save_ninja_result(trace, result, data)

        self.results = pd.DataFrame(results).set_index('trace')
        return self.results