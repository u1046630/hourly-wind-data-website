import os, json, glob
import pandas as pd
from datetime import datetime

from django.conf import settings

OUTPUT_DIR = os.path.join(settings.BASE_DIR, 'scripts/analysis/results/')

class Experiment(object):

    def __init__(self):
        '''setup'''
        assert(hasattr(self, 'exp_name')) # abstract, define in child class
        self.date_time = datetime.strftime(datetime.now(), '%Y-%m-%d %H-%M-%S')
        self.fname = os.path.join(OUTPUT_DIR, f'{self.exp_name} {self.date_time}.csv')

    def save(self):
        '''save most recently run results'''
        pd.DataFrame(self.results).to_csv(self.fname)