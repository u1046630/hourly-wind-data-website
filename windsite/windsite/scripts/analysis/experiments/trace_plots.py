
import pandas as pd, numpy as np, os
import progressbar
from .funcs import *
from farms.models import Trace, Farm, PowerCurve, SeriesNinjaCF
from scripts.analysis.experiments.base import Experiment

from scripts.analysis.experiments.funcs_ninja import ninja_wind_to_cf, ninja_adjustments

from django.conf import settings

class TracePlots(Experiment):

    def __init__(self):
        self.exp_name = 'trace_lots'
        super().__init__()

    def run(self):
        results = []
        traces = Trace.objects.all()

        for trace in progressbar.progressbar(traces):
            data = read_timeseries_csv(trace.data.file)
            save_trace_plot(trace, data)

            results.append({
                'trace': trace.name,
                'cf_mean': np.mean(data.cf_trace),
            })

        self.results = pd.DataFrame(results).set_index('trace')
        return self.results