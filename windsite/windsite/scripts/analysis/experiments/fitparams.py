
import pandas as pd, numpy as np, os
import progressbar
from .funcs import *
from farms.models import Trace, Farm, PowerCurve, SeriesEra5CF, ResultEra5
from scripts.analysis.experiments.base import Experiment

from django.conf import settings

OUTPUT_DIR = os.path.join(settings.BASE_DIR, 'scripts/analysis/results/')

class FitParams(Experiment):

    def __init__(self, exp_name):
        self.exp_name = exp_name
        super().__init__()

    def run(self):
        results = []
        traces = Trace.objects.all()

        fit_func = { # select appropriate function to use
            'era5_fit_dcurve': fit_dcurve,
            'era5_fit_timeseries': fit_timeseries,
        }[self.exp_name]

        for trace in progressbar.progressbar(traces):
            # if trace.name != 'Thorntonbank - Part 3':
            #     continue

            data_trace = read_timeseries_csv(trace.data.file)
            data_era5 = read_timeseries_csv(trace.farm.era5_wind.data.file)

            data = data_trace.join([data_era5], how='inner')
            data = data.sort_index()

            pcurve = read_pcurve(trace.farm.power_curve.data.file)

            params, perr = fit_func(data.cf_trace, data.wind, pcurve)
            wakeloss, sigma = params
            pcurve_adjusted = adjust_pcurve(pcurve, *params)
            data['cf_era5'] = wind_to_cf(data.wind, pcurve_adjusted)
            result = {
                'trace': trace.name,
                'wakeloss': wakeloss,
                'sigma': sigma,
                #'num_turbines': trace.farm.num_turbines,
                #'turbine_power': PowerCurve.objects.get(name=trace.farm.power_curve_era5).power,
            }
            result.update(calc_era5_errors(data))
            results.append(result)

            save_era5_result(trace, result, data, description=self.exp_name)

        self.results = pd.DataFrame(results).set_index('trace')
        return self.results      
        
class FitDcurve(FitParams):
    
    def __init__(self):
        super().__init__(exp_name='era5_fit_dcurve')

class FitTimeseries(FitParams):

    def __init__(self):
        super().__init__(exp_name='era5_fit_timeseries')