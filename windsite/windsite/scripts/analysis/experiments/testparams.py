
import pandas as pd, numpy as np, os
import progressbar
from .funcs import *
from farms.models import Trace, Farm, PowerCurve
from scripts.analysis.experiments.base import Experiment

from django.conf import settings

class TestParams(Experiment):

    def __init__(self, wakelosses, sigmas):
        self.exp_name = 'era5_test_params'
        self.wakelosses = wakelosses
        self.sigmas = sigmas
        super().__init__()

    def run(self):
        results = []
        traces = Trace.objects.all()

        for trace in progressbar.progressbar(traces):
            # if trace.name != 'Thorntonbank - Part 3':
            #     continue

            data_trace = read_timeseries_csv(trace.data.file)
            data_era5 = read_timeseries_csv(trace.farm.era5_wind.data.file)

            data = data_trace.join([data_era5], how='inner')
            data = data.sort_index()

            pcurve = read_pcurve(trace.farm.power_curve.data.file)

            for wakeloss in self.wakelosses:
                for sigma in self.sigmas:
                    pcurve_adjusted = adjust_pcurve(pcurve, wakeloss, sigma)
                    data['cf_era5'] = wind_to_cf(data.wind, pcurve_adjusted)
                    result = {
                        'trace': trace.name,
                        'wakeloss': wakeloss,
                        'sigma': sigma,
                        'wind_mean': np.mean(data.wind),
                        'wind_std': np.std(data.wind),
                        'cf_mean': np.mean(data.cf_era5),
                    }
                    result.update(calc_era5_errors(data))
                    results.append(result)

            if len(self.wakelosses) == 1 and len(self.sigmas) == 1:
                save_era5_result(trace, result, data, description='era5_test_params')

        self.results = pd.DataFrame(results).set_index('trace')
        return self.results