#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 17:57:39 2020

@author: liam
"""

import pandas as pd, numpy as np
from datetime import datetime, timedelta
from scripts.manage_era5.local_reader import get_wind_speed
from scripts.analysis.experiments.funcs import read_pcurve, adjust_pcurve, wind_to_cf
from scripts.analysis.experiments.funcs_ninja import ninja_wind_to_cf

pcurve_dir = '/media/liam/drive1/thewindpower/turbines/power-curve-data/'

date_from = datetime(2016, 1, 1)
date_to = datetime(2020, 1, 1)
height = 100
capacity = 30 # MW
location = 41.114711, -71.521161
#turbine is 'GE Energy Haliade 150', power curve not available
turbine = 'GE Energy 2.75-103'
#turbine = 'Vestas V112-3000'
wakeloss = 0.71
sigma = 1.17

def load_pcurve():
        pcurve = pd.read_csv(f'{pcurve_dir}{turbine}.csv').rename(columns={
                'Wind speed (m/s)': 'wind',
                'Power (kW)': 'power',
        })
        pcurve.power = pcurve.power / pcurve.power.max()
        return pcurve

pcurve_ninja = load_pcurve()
pcurve_era5 = load_pcurve()

df = pd.read_csv('scripts/analysis/block-island/ninja-wind-speed.csv').set_index('datetime')
df.index = pd.DatetimeIndex(df.index)
df = df.rename(columns={'wind_speed': 'wind_ninja'})
df['cf_ninja'] = ninja_wind_to_cf(df.wind_ninja, None, pcurve_ninja)
df['gen_ninja'] = df.cf_ninja * capacity

date_from = datetime(2016, 1, 1)
date_to = datetime(2020, 1, 1)

pcurve_era5 = adjust_pcurve(pcurve_era5, wakeloss, sigma)

df['wind_era5'] = get_wind_speed(date_from, date_to, height, location)
df['cf_era5'] = wind_to_cf(df.wind_era5, pcurve_era5)
df['gen_era5'] = df.cf_era5 * capacity

df['nhrs'] = 1

monthly = df.groupby(pd.Grouper(freq='M')).sum()
monthly.cf_era5 /= monthly.nhrs
monthly.cf_ninja /= monthly.nhrs
monthly.wind_era5 /= monthly.nhrs
monthly.wind_ninja /= monthly.nhrs
monthly.to_csv('scripts/analysis/block-island/block-island-generation2.csv')

