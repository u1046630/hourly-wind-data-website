from farms.models import Trace
import pandas as pd, numpy as np

print('name,num hrs,cf_trace,cf_era5,cf_ninja')

for t in Trace.objects.all():

    df = pd.read_csv(t.data).set_index('datetime')

    result_era5 = t.resultera5_set.last()
    result_ninja = t.resultninja_set.last()

    df = df.join(pd.read_csv(result_era5.era5_cf.data).set_index('datetime'))
    df = df.join(pd.read_csv(result_ninja.ninja_cf.data).set_index('datetime'))
    df = df[['cf_trace', 'cf_era5', 'cf_ninja']].dropna(how='any')
    df.index = pd.DatetimeIndex(df.index)
    n = len(df)
    df = df.mean()

    print('{},{},{},{},{}'.format(
        t.name,
        n,
        df.cf_trace,
        df.cf_era5,
        df.cf_ninja,
    ))

