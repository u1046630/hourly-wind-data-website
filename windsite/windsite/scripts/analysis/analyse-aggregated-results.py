
import pandas as pd, numpy as np, os
import matplotlib.pyplot as plt

fname = 'scripts/analysis/results/aggregated-unweighted.csv'

def load_csv(file):
    df = pd.read_csv(file).set_index('datetime')
    df.index = pd.DatetimeIndex(df.index)
    return df

def rmse(x, y):
    return np.sqrt(np.mean((x-y)**2))

if not os.path.exists(fname):
    raise Exception(f'[!] {fname} does not exist')
else:
    print(f'[*] loading aggregated results from "{fname}"')
    total = load_csv(fname)

results = []
for cutoff in range(int(total.weight.max()+1)):
    temp = total[total.weight >= cutoff]
    results.append({
        'cutoff': cutoff,
        'num_hrs': len(temp),
        'rmse_era5': rmse(temp.cf_trace, temp.cf_era5),
        'rmse_ninja': rmse(temp.cf_trace, temp.cf_ninja),
    })

results = pd.DataFrame(results).set_index('cutoff')
plt.plot(results.index, results.rmse_era5)
plt.plot(results.index, results.rmse_ninja)
plt.xlabel('minimum number of wind farms')
plt.ylabel('RMSE of aggregated prediction')
plt.legend(['ERA5', 'Ninja'])
plt.show()

plt.plot(results.index, results.num_hrs, color='black')
plt.xlabel('minimum number of wind farms')
plt.ylabel('number of hours of aggregation')
plt.show()
