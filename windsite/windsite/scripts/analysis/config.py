import numpy as np

config = {
    # used in the TestParams experiment
    'WAKELOSSES': [0.71], #[0.61, 0.63, 0.65, 0.67, 0.69, 0.71, 0.73, 0.75, 0.77, 0.79, 0.81], #np.arange(0, 1.5, 0.05),
    'SIGMAS': [2.24], #np.arange(2.0, 2.5, 0.04), #np.arange(2.1, 2.3, 0.02),

    # used in the AdjustLocation experiment
    'TRACE': 'Robin Rigg 2',
    'WAKELOSS': 0.71,
    'SIGMA': 2.24,
}

# wakeloss = 0.71 (when incremented by 0.01)
# sigma = 2.24 (when incremented by 0.02)


# renewables ninja wind speed adjustment parameters for offshore
ninja_adjustments = {
    'belgium': {'alpha': 0.625, 'alpha_offshore': 0.663, 'beta': 2.703},
    'denmark': {'alpha': 0.595, 'alpha_offshore': 0.633, 'beta': 2.846},
    'uk':      {'alpha': 0.642, 'alpha_offshore': 0.682, 'beta': 2.628},
}