


import pandas as pd, numpy as np, os, progressbar
from datetime import datetime, timedelta

from farms.models import Trace

fname = 'scripts/analysis/results/aggregated-unweighted.csv'

def load_csv(file):
    df = pd.read_csv(file).set_index('datetime')
    df.index = pd.DatetimeIndex(df.index)
    return df

if not os.path.exists(fname):
    print('[*] calculating wind farm aggregated results...', flush=True)

    index = pd.DatetimeIndex(np.arange(datetime(2014, 1, 1), datetime(2020, 1, 1), timedelta(hours=1)))

    total = None
    for t in progressbar.progressbar(list(Trace.objects.all())):
        df_trace = load_csv(t.data.file)
        df_era5 = load_csv(t.resultera5_set.last().era5_cf.data.file)
        df_ninja = load_csv(t.resultninja_set.last().ninja_cf.data.file)

        weight = df_trace.power.max() # or set to one to evenly weight the various wind farms
        weight = 1.0
        df_trace = df_trace[['cf_trace']]

        df = df_trace.join([df_era5, df_ninja], how='inner') * weight
        df['weight'] = weight
        df = df.reindex(index, fill_value=0)

        if total is None: 
            total = df
        else:
            total += df

    total = total[df.weight > 0]
    total.cf_trace = total.cf_trace / total.weight
    total.cf_era5 = total.cf_era5 / total.weight
    total.cf_ninja = total.cf_ninja / total.weight

    print(f'[*] saving to "{fname}"')
    total.to_csv(fname, index_label='datetime')

else:
    print(f'[*] loading previously calculated results from "{fname}"')
    total = load_csv(fname)

