'''
create polygon shapefile containing the area for which wind speed data is
avaliable on the website.

for each uploaded grid point, select a lat/lon to the NE and test if it lies in a covered grid cell
(all 4 surrounding grid points have been uploaded)
if so, make a shapely Polygon for that cell, careful to make sure the polygon doesn't wrap around lon==180 line
next step is to "dissolve" all grid cells together in QGIS
'''

import era5_mod, progressbar, pyproj
from shapely.geometry import Point, Polygon
import pandas as pd, geopandas as gpd

era5_reader = era5_mod.Reader('', {}) # dummy reader
N = era5_reader.n320.NUM_N320_PTS

fnames = [
    'scripts/point-lists/points-offshore-extended.csv',
]

pts = set()
for fname in fnames:
    df = pd.read_csv(fname)
    pts.update(df['FID'])

geometries = []
for i in progressbar.progressbar(pts):
    lat0, lon0 = era5_reader.n320.n320_to_latlon(i)
    lat0 += 0.1
    lon0 += 0.1
    weights = era5_reader.n320.latlon_to_n320_interpolated(lat0, lon0)
    if not all([j in pts for j in weights.keys()]):
        continue
    cell_coords = []
    lon_max = -360
    for j in weights.keys():
        lat, lon = era5_reader.n320.n320_to_latlon(j)
        if lon > 180:
            lon -= 360
        lon_max = max(lon, lon_max)
        cell_coords.append((lat, lon))
    cell_points = []
    for lat, lon in cell_coords:
        if lon_max - lon > 180:
            cell_points.append(Point(lon + 360, lat))
        else:
            cell_points.append(Point(lon, lat))

    # rely on the consistent order that latlon_to_n320_interpolated() provides
    cell_points[2], cell_points[3] = cell_points[3], cell_points[2]
    geometries.append(Polygon(cell_points))

gdf = gpd.GeoDataFrame(geometry=geometries)
gdf.to_file('out.shp')