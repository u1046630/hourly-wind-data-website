'''
create shapefile containing the ~500k points of the N320 reduced Gaussian grid
'''

import era5_mod, progressbar
from shapely.geometry import Point
import geopandas as gpd

era5_reader = era5_mod.Reader('', {}) # dummy reader
N = era5_reader.n320.NUM_N320_PTS

print('[*] creating multiprocessing pool...')
geometries = []
for i in progressbar.progressbar(list(range(N))):
    lat, lon = era5_reader.n320.n320_to_latlon(i)
    if lon > 180:
        lon -= 360
    geometries.append(Point(lon,lat))

gdf = gpd.GeoDataFrame(geometry=geometries)
gdf.to_file('scripts/data/all_points.shp')