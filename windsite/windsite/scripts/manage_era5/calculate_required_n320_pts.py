'''
if an N320 grid point is inside a "covered" region, than all of its 8 neighbours also need to 
be uploaded in order to interpolate to any point in the region. 

this script takes in some files listing N320 grid points that are inside the "covered" region, 
and output a file listing all of the points to upload.
'''

import era5_mod, progressbar, pyproj
from shapely.geometry import Point, Polygon
import pandas as pd, geopandas as gpd

era5_reader = era5_mod.Reader('', {}) # dummy reader
N = era5_reader.n320.NUM_N320_PTS
epsilon = 0.01

# covered regions:
fnames_in = [
    'scripts/point-lists/points-offshore.csv',
    'scripts/point-lists/points-chile.csv',
    'scripts/point-lists/points-bolivia.csv',
]
fname_out = 'scripts/point-lists/points-to-upload.csv'

pts_in = set()
for fname in fnames_in:
    df = pd.read_csv(fname)
    print(f'[*] {len(df)} initial points ({fname})', flush=True)
    pts_in.update(df['FID'])

pts_out = set(pts_in)
for i in progressbar.progressbar(pts_in):
    lat, lon = era5_reader.n320.n320_to_latlon(i)
    neighbours = set()
    weights = era5_reader.n320.latlon_to_n320_interpolated(lat + epsilon, lon + epsilon)
    neighbours.update(weights.keys())
    weights = era5_reader.n320.latlon_to_n320_interpolated(lat - epsilon, lon + epsilon)
    neighbours.update(weights.keys())
    weights = era5_reader.n320.latlon_to_n320_interpolated(lat + epsilon, lon - epsilon)
    neighbours.update(weights.keys())
    weights = era5_reader.n320.latlon_to_n320_interpolated(lat - epsilon, lon - epsilon)
    neighbours.update(weights.keys())
    pts_out.update(neighbours)

print(f'[*] {len(pts_out)} final points')

df = pd.DataFrame({'FID': sorted(list(pts_out))})
df.to_csv(fname_out)