'''
complete configuration for reading ERA5 wind speeds from local hard drive cache.

Read ERA5 wind data from local harddrive, using same method as 
the windsite.xyz reads it from the S3 bucket. A good way to
double check the website it behaving properly.
'''

import pandas as pd, numpy as np
import era5_mod
from datetime import datetime, timedelta

era5_dir = '/media/liam/drive1/era5/'
date = lambda x: datetime(x, 1, 1)
era5_config = {
    'level131-1980-1989/data/': (date(1980), date(1990), 131, ('u','v'), 484),
    'level131-1990-1999/data/': (date(1990), date(2000), 131, ('u','v'), 484),
    'level131-2000-2009/data/': (date(2000), date(2010), 131, ('u','v'), 484),
    'level131-2010-2019/data/': (date(2010), date(2020), 131, ('u','v'), 484),

    'level133-1980-1989/data/': (date(1980), date(1990), 133, ('u','v'), 484),
    'level133-1990-1993/data/': (date(1990), date(1994), 133, ('u','v'), 484),
    'level133-1994-2007/data/': (date(1994), date(2008), 133, ('u','v'), 484),
    'level133-2008-2009/data/': (date(2008), date(2010), 133, ('u','v'), 484),
    'level133-2010-2019/data/': (date(2010), date(2020), 133, ('u','v'), 484),

    'level135-1980-2009/data/': (date(1980), date(2010), 135, ('u','v'), 484),
    'level135-2010-2013/data/': (date(2010), date(2014), 135, ('u','v'), 484),
    'level135-2014-2019/data/': (date(2014), date(2020), 135, ('u','v'), 484),
}
era5_reader = era5_mod.Reader(era5_dir, era5_config)

def get_wind_speed(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # 1. combine components
    # 2. vertically interpolate
    # 3. horizontally interpolate

    wind = np.zeros(n, dtype='float16')

    for i_n320, weight in weights.items():

        u = era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320)
        wind131 = np.sqrt(u**2 + v**2)
        u = era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320)
        wind133 = np.sqrt(u**2 + v**2)
        u = era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320)
        wind135 = np.sqrt(u**2 + v**2)

        x = np.array([169.5, 106.54, 53.92])
        y = np.array([wind131, wind133, wind135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        wind += (a + b * np.log(height)) * weight

    return wind



if __name__ == '__main__':
    df = wind_speed(datetime(2019, 1, 2), datetime(2019, 1, 3), 100, 426751)
    print(df)