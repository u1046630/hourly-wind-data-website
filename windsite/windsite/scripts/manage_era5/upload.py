'''
upload 40 years of ERA5 wind speeds to S3 bucket.
data uploaded is the log fit (wind speed vs height) parameters.
multithreading to make sure upload bandwidth is always being used.
'''

import boto3, progressbar
from datetime import datetime
import numpy as np, pandas as pd
import os, threading

from local_reader import era5_reader

date = lambda x: datetime(x, 1, 1)

s3 = boto3.resource("s3")
bucket = s3.Bucket('windsite-files')

def upload_to_s3(bucket, fname, key):
    bucket.upload_file(Filename=fname, Key=key)
    os.remove(fname)

df = pd.read_csv('scripts/point-lists/points-offshore-extended.csv')
pts = df['FID']
print(f'[*] uploading data for {len(pts)} points')

year_ranges = [1980, 1990, 2000, 2010, 2020]

threads = []

for year_from, year_to in zip(year_ranges[:-1], year_ranges[1:]):
    print(f'[*] years {year_from} to {year_to}:', flush=True)

    prefix = f'era5/{year_from}-{year_to-1}'
    obj_list = {x.key for x in bucket.objects.filter(Prefix=prefix)}

    for i, i_n320 in progressbar.progressbar(list(enumerate(pts))):

        key_a = f'{prefix}/a/{i_n320}.npy'
        key_b = f'{prefix}/b/{i_n320}.npy'

        if key_a in obj_list and key_b in obj_list:
           continue # previously been uploaded

        date_from = date(year_from)
        date_to = date(year_to)

        u = era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320)
        wind131 = np.sqrt(u**2 + v**2)
        u = era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320)
        wind133 = np.sqrt(u**2 + v**2)
        u = era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320)
        wind135 = np.sqrt(u**2 + v**2)

        x = np.array([169.5, 106.54, 53.92])
        y = np.array([wind131, wind133, wind135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)

        fname_a = '/tmp/' + key_a.replace('/', '_')
        np.save(fname_a, a)
        fname_b = '/tmp/' + key_b.replace('/', '_')
        np.save(fname_b, b)

        for thread in threads:
            thread.join()
        threads = [
            threading.Thread(target=upload_to_s3, args=(bucket, fname_a, key_a)),
            threading.Thread(target=upload_to_s3, args=(bucket, fname_b, key_b)),
        ]
        for thread in threads:
            thread.start()

for thread in threads:
    thread.join()