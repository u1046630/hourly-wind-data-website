
'''
this script can't run, just a place to save the alternative methods for interpolating wind speeds.
"comb" means combine the wind components (east-west and north-south)
"vert" means interpolate vertically (fit log to wind speeds vs height)
"hori" means interpolate horizontally (inverse distance with four corners)
these operations can be in any order, my experiments show it really doesn't matter which one you pick,
but "comb_vect_hori" allows the smallest amount of data to be stored in S3 bucket.
'''

def comb_vert_hori(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # combine components
    # horizontally interpolate
    # vertically interpolate

    wind = np.zeros(n, dtype='float16')

    for i_n320, weight in weights.items():

        u = era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320)
        wind131 = np.sqrt(u**2 + v**2)
        u = era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320)
        wind133 = np.sqrt(u**2 + v**2)
        u = era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320)
        wind135 = np.sqrt(u**2 + v**2)

        x = np.array([169.5, 106.54, 53.92])
        y = np.array([wind131, wind133, wind135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        wind += (a + b * np.log(height)) * weight

    # PREVIOUS ERROR:
    # return (a + b * np.log(height)) * weight
    return wind


def hori_comb_vert(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # horizontally interpolate
    # combine components
    # vertically interpolate

    u131 = np.zeros(n, dtype='float16')
    u133 = np.zeros(n, dtype='float16')
    u135 = np.zeros(n, dtype='float16')
    v131 = np.zeros(n, dtype='float16')
    v133 = np.zeros(n, dtype='float16')
    v135 = np.zeros(n, dtype='float16')

    for i_n320, weight in weights.items():

        u131 += era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320) * weight
        u133 += era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320) * weight
        u135 += era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320) * weight
        v131 += era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320) * weight
        v133 += era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320) * weight
        v135 += era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320) * weight

    wind131 = np.sqrt(u131**2 + v131**2)
    wind133 = np.sqrt(u133**2 + v133**2)
    wind135 = np.sqrt(u135**2 + v135**2)
    x = np.array([169.5, 106.54, 53.92])
    y = np.array([wind131, wind133, wind135], dtype=np.float32)
    a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
    wind = a + b * np.log(height)

    return wind


def comb_hori_vert(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # combine components
    # horizontally interpolate
    # vertically interpolate

    wind131 = np.zeros(n, dtype='float16')
    wind133 = np.zeros(n, dtype='float16')
    wind135 = np.zeros(n, dtype='float16')

    for i_n320, weight in weights.items():

        u = era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320)
        wind131 += np.sqrt(u**2 + v**2) * weight
        u = era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320)
        wind133 += np.sqrt(u**2 + v**2) * weight
        u = era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320)
        v = era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320)
        wind135 += np.sqrt(u**2 + v**2) * weight

    x = np.array([169.5, 106.54, 53.92])
    y = np.array([wind131, wind133, wind135], dtype=np.float32)
    a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
    wind = a + b * np.log(height)

    return wind



def hori_vert_comb(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # horizontally interpolate
    # vertically interpolate
    # combine components

    u131 = np.zeros(n, dtype='float16')
    u133 = np.zeros(n, dtype='float16')
    u135 = np.zeros(n, dtype='float16')
    v131 = np.zeros(n, dtype='float16')
    v133 = np.zeros(n, dtype='float16')
    v135 = np.zeros(n, dtype='float16')

    for i_n320, weight in weights.items():

        u131 += era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320) * weight
        u133 += era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320) * weight
        u135 += era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320) * weight
        v131 += era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320) * weight
        v133 += era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320) * weight
        v135 += era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320) * weight

    x = np.array([169.5, 106.54, 53.92])

    y = np.array([u131, u133, u135], dtype=np.float32)
    u_a, u_b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
    u = u_a + u_b * np.log(height)

    y = np.array([v131, v133, v135], dtype=np.float32)
    v_a, v_b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
    v = v_a + v_b * np.log(height)

    wind = np.sqrt(u**2 + v**2)

    return wind



def vert_hori_comb(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # horizontally interpolate
    # vertically interpolate
    # combine components

    u = np.zeros(n, dtype='float16')
    v = np.zeros(n, dtype='float16')
    x = np.array([169.5, 106.54, 53.92])

    for i_n320, weight in weights.items():

        u131 = era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320)
        u133 = era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320)
        u135 = era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320)
        y = np.array([u131, u133, u135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        u += (a + b * np.log(height)) * weight

        v131 = era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320)
        v133 = era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320)
        v135 = era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320)
        y = np.array([v131, v133, v135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        v += (a + b * np.log(height)) * weight

    wind = np.sqrt(u**2 + v**2)

    return wind



def vert_comb_hori(date_from, date_to, height, location):

    if isinstance(location, int):
        i_n320 = location # location is n320 index (no interpolation)
        weights = {i_n320: 1.0}

    elif isinstance(location, tuple):
        lat, lon = location # location is lat,lon (use inverse distance interpolation)
        weights = era5_reader.n320.latlon_to_n320_interpolated(lat, lon)

    else:
        raise Exception('[!] location must be int or tuple')

    hour = timedelta(hours=1)
    n = (date_to - date_from) // hour

    # horizontally interpolate
    # vertically interpolate
    # combine components

    wind = np.zeros(n, dtype='float16')
    x = np.array([169.5, 106.54, 53.92])

    for i_n320, weight in weights.items():

        u131 = era5_reader.read_data(date_from, date_to, 131, 'u', n320_index=i_n320)
        u133 = era5_reader.read_data(date_from, date_to, 133, 'u', n320_index=i_n320)
        u135 = era5_reader.read_data(date_from, date_to, 135, 'u', n320_index=i_n320)
        y = np.array([u131, u133, u135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        u = (a + b * np.log(height))

        v131 = era5_reader.read_data(date_from, date_to, 131, 'v', n320_index=i_n320)
        v133 = era5_reader.read_data(date_from, date_to, 133, 'v', n320_index=i_n320)
        v135 = era5_reader.read_data(date_from, date_to, 135, 'v', n320_index=i_n320)
        y = np.array([v131, v133, v135], dtype=np.float32)
        a, b = np.polynomial.polynomial.polyfit(np.log(x), y, 1)
        v = (a + b * np.log(height))

        wind += np.sqrt(u**2 + v**2) * weight

    # PREVIOUS ERROR:
    #wind = np.sqrt(u**2 + v**2)

    return wind
