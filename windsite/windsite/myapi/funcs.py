
from scipy import ndimage

def adjust_power_curve(curve_x, curve_y, wakeloss, sigma):
    curve_x = curve_x + wakeloss
    curve_y = curve_y / curve_y.max()
    if sigma > 0:
        step = curve_x[1] - curve_x[0]
        sigma /= step
        curve_y = ndimage.filters.gaussian_filter1d(curve_y, sigma, mode='nearest')
    
    return curve_x, curve_y