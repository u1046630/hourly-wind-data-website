from django.conf import settings
import era5_mod
from datetime import datetime
date = lambda x: datetime(x, 1, 1)

import boto3
s3 = boto3.resource("s3")
bucket = s3.Bucket('windsite-files')
era5_data_dir = bucket
era5_config = {
    'era5/1980-1989/': (date(1980), date(1990)),
    'era5/1990-1999/': (date(1990), date(2000)),
    'era5/2000-2009/': (date(2000), date(2010)),
    'era5/2010-2019/': (date(2010), date(2020)),
}
era5_reader = era5_mod.NewReader(era5_data_dir, era5_config)

# era5_data_dir = '/media/liam/drive1/era5/'
# year = lambda x: datetime(x, 1, 1)
# era5_config = {
#     'level131-1980-1989/data/': (date(1980), date(1990), 131, ('u','v'), 484),
#     'level131-1990-1999/data/': (date(1990), date(2000), 131, ('u','v'), 484),
#     'level131-2000-2009/data/': (date(2000), date(2010), 131, ('u','v'), 484),
#     'level131-2010-2019/data/': (date(2010), date(2020), 131, ('u','v'), 484),

#     'level133-1980-1989/data/': (date(1980), date(1990), 133, ('u','v'), 484),
#     'level133-1990-1993/data/': (date(1990), date(1994), 133, ('u','v'), 484),
#     'level133-1994-2007/data/': (date(1994), date(2008), 133, ('u','v'), 484),
#     'level133-2008-2009/data/': (date(2008), date(2010), 133, ('u','v'), 484),
#     'level133-2010-2019/data/': (date(2010), date(2020), 133, ('u','v'), 484),

#     'level135-1980-2009/data/': (date(1980), date(2010), 135, ('u','v'), 484),
#     'level135-2010-2013/data/': (date(2010), date(2014), 135, ('u','v'), 484),
#     'level135-2014-2019/data/': (date(2014), date(2020), 135, ('u','v'), 484),
# }
# era5_reader = era5_mod.Reader(era5_data_dir, era5_config)



    