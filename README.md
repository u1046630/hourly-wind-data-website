
# Hourly Wind Data - Website

The aim is to make the hourly wind data easily avaliable, as well as the research and methods behind it. 

Python version used: 3.8.2 (set with pyenv)

## Poetry

- Each subdirectory is a python package project managed by poetry. 
- `era5_mod`: `/home/liam/.cache/pypoetry/virtualenvs/era5-mod-rb8ekJgw-py3.8`
- `windsite`: `/home/liam/.cache/pypoetry/virtualenvs/windsite-jFY19ySg-py3.8`
- `gwa_mod`: `/home/liam/.cache/pypoetry/virtualenvs/gwa-mod-vgrEsBJK-py3.8`

## Command quick reference

```
pyenv local 3.8.2
poetry new era5_mod
poetry add <package>
```

# Uploaded ERA5 data:
name = 'bolivia'
lat_min, lat_max = -23.079732,-9.514079
lon_min, lon_max = -70.092773,-57.392578

name = 'uk'
lat_min, lat_max = 49.60185302194436 59.1568794292387
lon_min, lon_max = -6.75 4.21875

# To get an 18.04 Ubuntu EC2 instance up and running:

## Make life easier
/etc/hosts:
    54.206.45.39	ec2
~/.ssh/config:
    Host 54.206.45.39 ec2
        User ubuntu
        IdentityFile ~/.ssh/aws-pikkabird-key.pem

## repo
git clone https://gitlab.anu.edu.au/u1046630/hourly-wind-data-website.git
cd hourly-wind-data-website

## pip
sudo apt update
sudo apt install python3-pip

## python (build dependencies for pyenv)

sudo apt install libffi-dev (must do before building python with "pyenv install ..." to avoid "no module named _ctypes" error)
see here: https://stackoverflow.com/questions/27022373/python3-importerror-no-module-named-ctypes-when-using-value-from-module-mul

(see https://github.com/pyenv/pyenv/wiki/Common-build-problems)
sudo apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python-openssl git

## pyenv
curl https://pyenv.run | bash
<add things to .bashrc>
pyenv install 3.8.2
source ~/.bashrc
pyenv --version (1.2.29)
pyenv versions

## poetry
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3
add `source ~/.poetry/env` to ~/.bashrc
source ~/.bashrc
poetry --version (1.0.9)
poetry install

## PostgreSQL
see here: https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04
sudo apt install postgresql postgresql-contrib
poetry add psycopg2
sudo -u postgres psql
CREATE DATABASE myproject;
CREATE USER myprojectuser WITH PASSWORD 'password';
ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE myprojectuser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;
\q

Change database in django settings:
'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'myproject',
    'USER': 'myprojectuser',
    'PASSWORD': 'password',
    'HOST': 'localhost',
    'PORT': '',
}



## S3 creds
scp -r ~/.aws ec2:~/

## Move data from local Django database
./manage.py dumpdata --natural-primary --natural-foreign > datadumps/all-natural.json
scp datadumps/all-natural.json ec2:~/datadumps/
scp media.zip ec2:~/hourly-wind-data-website/windsite/windsite/media.zip; unzip media.zip; rm media.zip
OR
rsync -rvv --delete media/ ec2:~/hourly-wind-data-website/windsite/windsite/media/

ON THE REMOTE MACHINE:
./manage.py flush
git pull
./manage.py migrate
./manage.py loaddata ~/datadumps/all-natural.json


## Move media files
rsync -vvr media/ ec2:~/hourly-wind-data-website/windsite/windsite/media/


## Gunicorn
poetry add gunicorn
tutorial: https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04

### gunicorn.socket
sudo vim /etc/systemd/system/gunicorn.socket
```
    [Unit]
    Description=gunicorn socket

    [Socket]
    ListenStream=/run/gunicorn.sock

    [Install]
    WantedBy=sockets.target
```
sudo systemctl start gunicorn.socket
sudo systemctl enable gunicorn.socket
sudo systemctl status gunicorn.socket
file /run/gunicorn.sock (should be socket)

### gunicorn.service
sudo vim /etc/systemd/system/gunicorn.service
```
    [Unit]
    Description=gunicorn daemon
    Requires=gunicorn.socket
    After=network.target

    [Service]
    User=ubuntu
    Group=www-data
    WorkingDirectory=/home/ubuntu/hourly-wind-data-website/windsite/windsite
    ExecStart=/home/ubuntu/.cache/pypoetry/virtualenvs/windsite-FropniS8-py3.8/bin/gunicorn \
            --access-logfile - \
            --workers 3 \
            --bind unix:/run/gunicorn.sock \
            --env DJANGO_LOCATION=ec2 \
            windsite.wsgi:application

    [Install]
    WantedBy=multi-user.target
```
sudo systemctl status gunicorn
curl --unix-socket /run/gunicorn.sock localhost
to make changes to .service file:
sudo systemctl daemon-reload
sudo systemctl restart gunicorn

when changes are made to django:
git pull
sudo service gunicorn restart

### Nginx

sudo vim /etc/nginx/sites-enabled/windsite
```
server {
    listen 8000;
    server_name 172.31.25.208;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/ubuntu/hourly-wind-data-website/windsite/windsite;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
```
sudo nginx -t # check for config errors
sudo systemctl restart nginx
sudo systemctl status nginx





## Running analysis

./manange shell
>>> run scripts/manage_data/remove_validation_results.py
>>> run scripts/manage_data/insert_data.py
./scripts/analysis/run.py ninja
./scripts/analysis/run.py era5 0.71 1.17
./scripts/analysis/run.py trace_plots
./manange shell
>>> rm scripts/analysis/results/aggregated-*.csv
>>> run scripts/analysis/calculate-aggregated-results.py
>>> 

# Mounting harddrive into Windows bash prompt

sudo mount -t drvfs F: /mnt/f






